<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 * Fecha
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\FechaRepository")
 */
class Fecha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dia", type="string", length=50)
     */
    private $dia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_inicio", type="time")
     * @Assert\NotNull()
     */
    private $horaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_fin", type="time")
     * @Assert\NotNull()
     */
    private $horaFin;

    /**
     * @ORM\ManyToOne(targetEntity="Calendario", inversedBy="fechas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendario_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $calendario;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->horaInicio = new \DateTime('today');
        $this->horaFin = new \DateTime('today');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dia.
     *
     * @param string $dia
     *
     * @return Fecha
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia.
     *
     * @return string
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Set horaInicio.
     *
     * @param \DateTime $horaInicio
     *
     * @return Fecha
     */
    public function setHoraInicio($horaInicio)
    {
        $this->horaInicio = $horaInicio;

        return $this;
    }

    /**
     * Get horaInicio.
     *
     * @return \DateTime
     */
    public function getHoraInicio()
    {
        return $this->horaInicio;
    }

    /**
     * Set horaFin.
     *
     * @param \DateTime $horaFin
     *
     * @return Fecha
     */
    public function setHoraFin($horaFin)
    {
        $this->horaFin = $horaFin;

        return $this;
    }

    /**
     * Get horaFin.
     *
     * @return \DateTime
     */
    public function getHoraFin()
    {
        return $this->horaFin;
    }

    /**
     * Set calendario.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Calendario $calendario
     *
     * @return Fecha
     */
    public function setCalendario(\CmsGa\CalendarioBundle\Entity\Calendario $calendario)
    {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Get calendario.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Calendario
     */
    public function getCalendario()
    {
        return $this->calendario;
    }
}
