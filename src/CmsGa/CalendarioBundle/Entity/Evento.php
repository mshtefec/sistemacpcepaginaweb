<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

// use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Evento.
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\EventoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Evento extends Calendario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    protected $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=true)
     */
    protected $contenido;

    /**
     *  @ORM\OneToMany(targetEntity="ImagenEvento", mappedBy="evento", cascade={"all"}, orphanRemoval=true)
     */
    protected $imagenes;

    /**
     * @var int
     * 
     * @ORM\OneTomany(targetEntity="VideoEvento",mappedBy="evento", cascade={"all"}, orphanRemoval=true)    
     */
    protected $videos;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->imagenes = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->fechas = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Evento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set contenido.
     *
     * @param string $contenido
     *
     * @return Evento
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido.
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Add imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     *
     * @return Evento
     */
    public function addImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $imagenes->setEvento($this);
        $this->imagenes[] = $imagenes;

        return $this;
    }

    /**
     * Remove imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     */
    public function removeImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $this->imagenes->removeElement($imagenes);
    }

    /**
     * Get imagenes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Add videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     *
     * @return Evento
     */
    public function addVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $videos->setEvento($this);
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     */
    public function removeVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }
}
