<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curso.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CursoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Curso extends Calendario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    protected $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=true)
     */
    protected $contenido;

    /**
     * @var int
     *
     * @ORM\OneTomany(targetEntity="ImagenEvento",mappedBy="curso", cascade={"all"}, orphanRemoval=true)  
     */
    protected $imagenes;

    /**
     * @var int
     *
     * @ORM\OneTomany(targetEntity="VideoEvento",mappedBy="curso", cascade={"all"}, orphanRemoval=true)  
     */
    protected $videos;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->videos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set contenido.
     *
     * @param string $contenido
     *
     * @return Curso
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido.
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Add imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     *
     * @return Curso
     */
    public function addImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $imagenes->setCurso($this);
        $this->imagenes[] = $imagenes;

        return $this;
    }

    /**
     * Remove imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     */
    public function removeImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $this->imagenes->removeElement($imagenes);
    }

    /**
     * Get imagenes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Add videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     *
     * @return Curso
     */
    public function addVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     */
    public function removeVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }
}
