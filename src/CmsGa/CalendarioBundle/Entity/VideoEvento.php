<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video.
 *
 * @ORM\Table(name="video_evento")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class VideoEvento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoIframe", type="text")
     */
    private $codigoIframe;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Evento", inversedBy="videos")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=true)
     */
    private $evento;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Actividad", inversedBy="videos")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", nullable=true)
     */
    private $actividad;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="videos")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id", nullable=true)
     */
    private $curso;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoIframe.
     *
     * @param string $codigoIframe
     *
     * @return VideoEvento
     */
    public function setCodigoIframe($codigoIframe)
    {
        $this->codigoIframe = $codigoIframe;

        return $this;
    }

    /**
     * Get codigoIframe.
     *
     * @return string
     */
    public function getCodigoIframe()
    {
        return $this->codigoIframe;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return VideoEvento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set evento.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Evento $evento
     *
     * @return VideoEvento
     */
    public function setEvento(\CmsGa\CalendarioBundle\Entity\Evento $evento = null)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set actividad.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Actividad $actividad
     *
     * @return VideoEvento
     */
    public function setActividad(\CmsGa\CalendarioBundle\Entity\Actividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set curso.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Curso $curso
     *
     * @return VideoEvento
     */
    public function setCurso(\CmsGa\CalendarioBundle\Entity\Curso $curso = null)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Curso
     */
    public function getCurso()
    {
        return $this->curso;
    }
}
