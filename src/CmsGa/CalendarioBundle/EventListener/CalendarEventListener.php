<?php

namespace CmsGa\CalendarioBundle\EventListener;

use ADesigns\CalendarBundle\Event\CalendarEvent;
use ADesigns\CalendarBundle\Entity\EventEntity;
use Doctrine\ORM\EntityManager;

class CalendarEventListener
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadEvents(CalendarEvent $calendarEvent)
    {
        $startDate = $calendarEvent->getStartDatetime();
        $endDate = $calendarEvent->getEndDatetime();

        // The original request so you can get filters from the calendar
        // Use the filter in your query for example

        $request = $calendarEvent->getRequest();
        $filter = $request->get('filter');

        // load events using your custom logic here,
        // for instance, retrieving events from a repository

        // $companyEvents = $this->entityManager->getRepository('MwsHCSIpapBundle:Calendario')
        //     ->createQueryBuilder('company_events')
        //     ->where('company_events.startDatetime BETWEEN :startDate and :endDate')
        //     ->orWhere('company_events.endDatetime BETWEEN :startDate and :endDate')
        //     ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
        //     ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
        //     ->getQuery()
        //     ->getResult()
        // ;
        $companyEvents = $this->entityManager->getRepository('CmsGaCalendarioBundle:Calendario')
            ->findSegunRango($startDate, $endDate);

        // $companyEvents and $companyEvent in this example
        // represent entities from your database, NOT instances of EventEntity
        // within this bundle.
        //
        // Create EventEntity instances and populate it's properties with data
        // from your own entities/database values.

        foreach ($companyEvents as $companyEvent) {

            // create an event with a start/end time, or an all day event
            if ($companyEvent->getAllDay() === true) {
                $this->addEvent($calendarEvent, $companyEvent, $companyEvent->getStartDatetime(), $companyEvent->getEndDatetime());
            } else {
                $fechas = $companyEvent->getFechas();
                $dias_semana = array();
                $dia_hora = array();
                foreach ($fechas as $fecha) {
                    array_push($dias_semana, $fecha->getDia());
                    $dia_hora[$fecha->getDia()]['H'] = $fecha->getHoraInicio()->format('H');
                    $dia_hora[$fecha->getDia()]['i'] = $fecha->getHoraInicio()->format('i');
                    $dia_hora[$fecha->getDia()]['s'] = $fecha->getHoraInicio()->format('s');
                }
                $intervalo_dias = new \DateInterval('P1D');
                $dias = new \DatePeriod($companyEvent->getStartDatetime(), $intervalo_dias, $companyEvent->getEndDatetime());
                foreach ($dias as $dia) {
                    if (in_array($dia->format('D'), $dias_semana)) {
                        /*$dia->setTime(
                            $dia_hora[$dia->format('D')]['H'],
                            $dia_hora[$dia->format('D')]['i'],
                            $dia_hora[$dia->format('D')]['s']
                        );*/
                         $dia->modify("+{$dia_hora[$dia->format('D')]['H']} hours");
                        $dia->modify("+{$dia_hora[$dia->format('D')]['i']} minutes");
                        $dia->modify("+{$dia_hora[$dia->format('D')]['s']} seconds");
                        $this->addEvent($calendarEvent, $companyEvent, $dia, null, true);
                    }
                }
                $ultimo_dia = $companyEvent->getEndDatetime();
                if (in_array($ultimo_dia->format('D'), $dias_semana)) {
                    $ultimo_dia->setTime(
                        $dia_hora[$ultimo_dia->format('D')]['H'],
                        $dia_hora[$ultimo_dia->format('D')]['i'],
                        $dia_hora[$ultimo_dia->format('D')]['s']
                    );
                    $this->addEvent($calendarEvent, $companyEvent, $ultimo_dia, null, true);
                }
            }
        }
    }

    private function addEvent($calendarEvent, $companyEvent, $start, $end = null, $allDay = false)
    {
        $eventEntity = new EventEntity($companyEvent->getTitle(), $start, $end, $allDay);
        //optional calendar event settings
        $eventEntity->setAllDay($companyEvent->getAllDay()); // default is false, set to true if this is an all day event
        $eventEntity->setBgColor('#'.$companyEvent->getBgColor()); //set the background color of the event's label
        $eventEntity->setFgColor('#'.$companyEvent->getFgColor()); //set the foreground color of the event's label
        $eventEntity->setUrl($companyEvent->getUrl()); // url to send user to when event label is clicked
        $eventEntity->setCssClass('my-custom-class'); // a custom class you may want to apply to event labels

        //finally, add the event to the CalendarEvent for displaying on the calendar
        $calendarEvent->addEvent($eventEntity);
    }
}
