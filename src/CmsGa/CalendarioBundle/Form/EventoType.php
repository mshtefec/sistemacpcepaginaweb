<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * EventoType form.
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class EventoType extends CalendarioType {

    public function __construct($startEndDatetime = null, $manager) {
        $this->startEndDatetime = $startEndDatetime;
        parent::__construct($manager);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);

        if (is_null($this->startEndDatetime)) {
            $builder->add('startEndDatetime', 'text', array(
                'mapped' => false,
                'label' => 'Fecha inicio - fin',
                'attr' => array(
                    // 'readonly' => 'readonly',
                    'data-format' => 'MM/dd/yyyy',
                ),
            ));
        } else {
            $builder->add('startEndDatetime', 'text', array(
                'mapped' => false,
                'label' => 'Fecha inicio - fin',
                'attr' => array(
                    // 'readonly' => 'readonly',
                    'data-format' => 'MM/dd/yyyy',
                    'value' => $this->startEndDatetime,
                ),
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Evento',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_evento';
    }

}
