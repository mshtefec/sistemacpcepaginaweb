<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * CursoFilterType filtro.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class CursoFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'filter_number_range')
            ->add('descripcion', 'filter_text')
            ->add('contenido', 'filter_text')
            ->add('title', 'filter_text')
            ->add('url', 'filter_text')
            ->add('bgColor', 'filter_text')
            ->add('fgColor', 'filter_text')
            ->add('cssClass', 'filter_text')
            ->add('startDatetime', 'filter_date_range')
            ->add('endDatetime', 'filter_date_range')
            ->add('allDay', 'filter_choice')
            ->add('updatedAt', 'filter_date_range')
            ->add('createdAt', 'filter_date_range')
            ->add('activo', 'filter_choice')
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Curso',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_calendariobundle_cursofiltertype';
    }
}
