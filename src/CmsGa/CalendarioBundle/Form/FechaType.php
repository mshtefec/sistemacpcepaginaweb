<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * EventoType form.
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class FechaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dia', 'choice', array(
                    'choices' => $this->getDias(),
                ))
                ->add('horaInicio', 'time')
                ->add('horaFin', 'time')
        /* ->add('horaInicio', 'collot_datetime', array(
          'pickerOptions' => array(
          'format' => 'hh:ii',
          'minView' => 'hour',
          'startView' => 1,
          ),
          'attr' => array(
          'readonly' => 'readonly',
          ),
          ))
          ->add('horaFin', 'collot_datetime', array(
          'pickerOptions' => array(
          'format' => 'hh:ii',
          'minView' => 'hour',
          'startView' => 1,
          ),
          'attr' => array(
          'readonly' => 'readonly',
          ),
          )) */
        // ->add('calendario')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Fecha',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_fecha';
    }

    private function getDias() {
        return array(
            'Mon' => 'Lunes',
            'Tue' => 'Martes',
            'Wed' => 'Miercoles',
            'Thu' => 'Jueves',
            'Fri' => 'Viernes',
            'Sat' => 'Sabado',
            'Sun' => 'Domingo',
        );
    }

}
