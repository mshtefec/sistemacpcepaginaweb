<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * EventoFilterType filtro.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class EventoFilterType extends AbstractType
{
    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title', 'filter_text', array(
                'label' => 'Título',
                )
            )
            ->add(
                'url', 'filter_text', array(
                'label' => 'URL',
                )
            )
            //->add('startDatetime', 'filter_date_range')
            //->add('endDatetime', 'filter_date_range')
            ->add(
                'allDay', 'filter_choice', array(
                'label' => 'Todo el día',
                'choices' => array(
                    1 => 'Si',
                    0 => 'No',
                    ),
                )
            )
            ->add(
                'descripcion', 'filter_text', array(
                'label' => 'Descripción',
                )
            )
            ->add(
                'activo', 'filter_choice', array(
                'label' => 'Activo',
                'choices' => array(
                    1 => 'Si',
                    0 => 'No',
                    ),
                )
            );

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Evento',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'CmsGa_CalendarioBundle_eventofiltertype';
    }
}
