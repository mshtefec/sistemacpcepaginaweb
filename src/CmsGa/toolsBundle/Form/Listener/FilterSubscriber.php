<?php

namespace CmsGa\toolsBundle\Form\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Lexik\Bundle\FormFilterBundle\Event\ApplyFilterEvent;

class FilterSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'lexik_form_filter.apply.orm.filter_text' => array('filterTextLike'),
            'lexik_form_filter.apply.orm.text' => array('filterTextLike'),
            'lexik_form_filter.apply.dbal.filter_text' => array('filterTextLike'),
            'lexik_form_filter.apply.dbal.text' => array('filterTextLike'),
        );
    }

   /**
    * Apply a filter for a filter_locale type.
    *
    * This method should work whih both ORM and DBAL query builder.
    */
   public function filterTextLike(ApplyFilterEvent $event)
   {
       $qb = $event->getQueryBuilder();
       $expr = $event->getFilterQuery()->getExpressionBuilder();
       $values = $event->getValues();

       if ('' !== $values['value'] && null !== $values['value']) {
           if (isset($values['condition_pattern'])) {
               $qb->andWhere($expr->stringLike($event->getField(), '%'.$values['value'].'%', $values['condition_pattern']));
           } else {
               $qb->andWhere($expr->stringLike($event->getField(), '%'.$values['value'].'%'));
           }
       }
   }
}
