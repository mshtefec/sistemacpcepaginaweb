<?php

namespace CmsGa\toolsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use CmsGa\toolsBundle\Form\DataTransformer\EntityToIdTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class EntityHiddenType.
 *
 * @author Björn Fromme <mail@bjo3rn.com>
 */
class EntityHiddenType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Constructor.
     *
     * @param ObjectManager $objectManager Object Manager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder Form Builder Interface
     * @param array                $options Opciones
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIdTransformer($this->objectManager, $options['class']);
        $builder->addModelTransformer($transformer);
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver Resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'class' => null,
                'invalid_message' => 'The entity does not exist.',
            )
        );
    }

    /**
     * Get Parent.
     *
     * @return null|string|\Symfony\Component\Form\FormTypeInterface
     */
    public function getParent()
    {
        return 'hidden';
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return 'entity_hidden';
    }
}
