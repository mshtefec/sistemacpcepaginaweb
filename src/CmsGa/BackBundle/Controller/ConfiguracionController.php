<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Configuracion;
use CmsGa\BackBundle\Form\ConfiguracionType;
use CmsGa\BackBundle\Form\ConfiguracionFilterType;

/**
 * Configuracion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/configuracion")
 */
class ConfiguracionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Configuracion.yml',
    );

    /**
     * Lists all Configuracion entities.
     *
     * @Route("/", name="admin_configuracion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ConfiguracionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Configuracion entity.
     *
     * @Route("/", name="admin_configuracion_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Configuracion:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ConfiguracionType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Configuracion entity.
     *
     * @Route("/new", name="admin_configuracion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ConfiguracionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Configuracion entity.
     *
     * @Route("/{id}", name="admin_configuracion_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Configuracion entity.
     *
     * @Route("/{id}/edit", name="admin_configuracion_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ConfiguracionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Configuracion entity.
     *
     * @Route("/{id}", name="admin_configuracion_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Configuracion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ConfiguracionType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Configuracion entity.
     *
     * @Route("/{id}", name="admin_configuracion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Configuracion.
     *
     * @Route("/exporter/{format}", name="admin_configuracion_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Configuracion.
     *
     * @Route("/get-table/", name="admin_configuracion_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}