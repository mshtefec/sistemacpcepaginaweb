<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Contacto;
use CmsGa\BackBundle\Form\ContactoType;
use CmsGa\BackBundle\Form\ContactoFilterType;

/**
 * Contacto controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/contacto")
 */
class ContactoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Contacto.yml',
    );

    /**
     * Lists all Contacto entities.
     *
     * @Route("/", name="admin_contacto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ContactoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Contacto entity.
     *
     * @Route("/", name="admin_contacto_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Contacto:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ContactoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Contacto entity.
     *
     * @Route("/new", name="admin_contacto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ContactoType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Contacto entity.
     *
     * @Route("/{id}", name="admin_contacto_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Contacto entity.
     *
     * @Route("/{id}/edit", name="admin_contacto_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ContactoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Contacto entity.
     *
     * @Route("/{id}", name="admin_contacto_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Contacto:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ContactoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Contacto entity.
     *
     * @Route("/{id}", name="admin_contacto_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Contacto.
     *
     * @Route("/exporter/{format}", name="admin_contacto_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Contacto entity.
     *
     * @Route("/autocomplete-forms/get-correo", name="Contacto_autocomplete_correo")
     */
    public function getAutocompleteCorreo()
    {
        $options = array(
            'repository' => "CmsGaBackBundle:Correo",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Contacto.
     *
     * @Route("/get-table/", name="admin_contacto_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}