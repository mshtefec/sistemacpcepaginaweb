<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Noticia;
use CmsGa\BackBundle\Form\NoticiaType;
use CmsGa\BackBundle\Form\NoticiaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Noticia controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/noticia")
 */
class NoticiaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Noticia.yml',
    );

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();

        $qb
                ->select('a.id', 'i.name', 'a.titulo', 'a.descripcion', 'a.fecha', 'a.publicado', 'a.portada', 'a.updatedAt', 'a.createdAt')
                ->from($config['repository'], 'a')
                ->leftJoin('a.seccion', 'seccion')
                ->leftJoin('seccion.item', 'i')
        ;
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_CONTROL')) {
            $qb
                    ->Join('seccion.usuarios', 'u')
            ;
        }
        $tipoArray[] = array(
            'column' => 4,
            'type' => 'datetime',
            'format' => 'd/m/Y H:i:s'
        );
        $tipoArray[] = array(
            'column' => 7,
            'type' => 'datetime',
            'format' => 'd/m/Y H:i:s'
        );
        $tipoArray[] = array(
            'column' => 8,
            'type' => 'datetime',
            'format' => 'd/m/Y H:i:s'
        );
        $array = array(
            'query' => $qb,
            'tipoArray' => $tipoArray
        );

        return $array;
    }

    /**
     * Lists all Noticia entities.
     *
     * @Route("/", name="admin_noticia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new NoticiaFilterType();
        $response = parent::indexAction();


        return $response;
    }

    /**
     * Creates a new Noticia entity.
     *
     * @Route("/", name="admin_noticia_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Noticia:new.html.twig")
     */
    public function createAction() {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //archivo
            if (!is_null($entity->getArchivo())) {
                if (is_null($entity->getArchivo()->getFile())) {
                    $entity->setArchivo(null);
                }
            }
            //imagen portada
            if (is_null($entity->getImagenPortada()->getFile())) {
                $entity->setImagenPortada(null);
            }
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');
            //Si selecciono una pagina de face y publicado esta en true entra
            if (!is_null($form->get('idPage')->getData()) && $entity->getPublicado()) {
                $paginaFace = $em->getRepository('CmsGaBackBundle:PageFace')->find($form->get('idPage')->getData());
                $idPostFace = $this->postInFacebook($paginaFace, $entity);
            }
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $em = $this->getDoctrine()->getManager();
        $configuraciones = array();
        $permitirNoticia = current($em->getRepository('CmsGaBackBundle:Configuracion')->findByClave("PermisoNoticia"));
        $configuraciones["PermisoNoticia"] = $permitirNoticia->getValor();
        $form = $this->createForm(new NoticiaType($this->get('security.authorization_checker'), $configuraciones, $this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    )
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array(
                        'class' => 'form-control btn-primary',
                        'col' => 'col-lg-3',
                    )
                ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Noticia entity.
     *
     * @Route("/new", name="admin_noticia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {

        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Noticia entity.
     *
     * @Route("/{id}", name="admin_noticia_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Noticia entity.
     *
     * @Route("/{id}/edit", name="admin_noticia_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $config = $this->getConfig();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->findNoticia($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }

        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($config, $entity) {
        $em = $this->getDoctrine()->getManager();
        $configuraciones = array();
        $permitirNoticia = current($em->getRepository('CmsGaBackBundle:Configuracion')->findByClave("PermisoNoticia"));
        $configuraciones["PermisoNoticia"] = $permitirNoticia->getValor();
        $form = $this->createForm(new NoticiaType($this->get('security.authorization_checker'), $configuraciones, $this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    )
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array(
                        'class' => 'form-control btn-primary',
                        'col' => 'col-lg-3',
                    )
                ))
        ;

        return $form;
    }

    /**
     * Edits an existing Noticia entity.
     *
     * @Route("/{id}", name="admin_noticia_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Noticia:edit.html.twig")
     */
    public function updateAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $values = $request->request->all();
            //archivo
            if (isset($values['cmsga_backbundle_noticia']['archivo_eliminar'])) {
                $existArchivo = $editForm->get('archivo_eliminar')->getData();
            } else {
                $existArchivo = null;
            }

            if (!is_null($existArchivo)) {
                if ($existArchivo) {
                    $em->remove($entity->getArchivo());
                }
            }
            if (!is_null($entity->getArchivo())) {
                if (is_null($entity->getArchivo()->getFile()) == true && is_null($entity->getArchivo()->getFilePath()) == true) {
                    $entity->setArchivo(null);
                }
            }
            //imagen portada 
            if (isset($values['cmsga_backbundle_noticia']['imagenPortada_eliminar'])) {
                $existImagenPortada = $editForm->get('imagenPortada_eliminar')->getData();
            } else {
                $existImagenPortada = null;
            }
            if (!is_null($existImagenPortada)) {
                if ($existImagenPortada) {
                    $em->remove($entity->getImagenPortada());
                }
            }

            if (is_null($entity->getImagenPortada()->getFile()) == true && is_null($entity->getImagenPortada()->getFilePath()) == true) {
                $entity->setImagenPortada(null);
            }
            if ($this->container->get('security.authorization_checker')->isGranted('ROLE_CONTROL') == false) {
                $entity->setPublicado(false);
            }
            $em->flush();
            //Si selecciono una pagina de face y publicado esta en true entra
            if (!is_null($editForm->get('idPage')->getData()) && $entity->getPublicado()) {
                $paginaFace = $em->getRepository('CmsGaBackBundle:PageFace')->find($editForm->get('idPage')->getData());
                $idPostFace = $this->postInFacebook($paginaFace, $entity);
            }
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Noticia entity.
     *
     * @Route("/{id}", name="admin_noticia_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Noticia.
     *
     * @Route("/exporter/{format}", name="admin_noticia_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-imagenes", name="Noticia_autocomplete_imagenes")
     */
    public function getAutocompleteImagenNoticia() {
        $options = array(
            'repository' => "CmsGaBackBundle:ImagenNoticia",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-videos", name="Noticia_autocomplete_videos")
     */
    public function getAutocompleteVideoNoticia() {
        $options = array(
            'repository' => "CmsGaBackBundle:VideoNoticia",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-seccion", name="Noticia_autocomplete_seccion")
     */
    public function getAutocompleteSeccion() {
        $options = array(
            'repository' => "CmsGaBackBundle:Seccion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-imagenPortada", name="Noticia_autocomplete_imagenPortada")
     */
    public function getAutocompleteImagenPortada() {
        $options = array(
            'repository' => "CmsGaBackBundle:ImagenPortada",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-archivo", name="Noticia_autocomplete_archivo")
     */
    public function getAutocompleteArchivo() {
        $options = array(
            'repository' => "CmsGaRepositoryFilesBundle:Archivo",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Noticia.
     *
     * @Route("/get-table/", name="admin_noticia_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Get Url.
     *
     * @param  type $var description
     *                    
     * @return $url
     */
    public function getCompleteUrl($id) {
        $request = $this->getRequest();
        $urlDetalle = $this->generateUrl('detalle_noticia_back', array('id' => $id)); //this->container
        /* ->get('router')                 
          ->generate(
          'detalle_noticia', array(
          'id', $id
          )
          ); */
        $url = $request->getHttpHost()
                . $request->getBasePath()
                . $urlDetalle;

        return $url;
    }

    /**
     * Finds and displays a Noticia entity.
     *
     * @Route("/habilitar-noticias/", name="noticia_habilitar")
     * @Method("GET")
     * @Template()
     */
    public function habilitarNoticiasAction() {
        $em = $this->getDoctrine()->getManager();

        $entitys = $em->getRepository('CmsGaBackBundle:Noticia')->findNoticiaPorAutorizar();

        return array(
            'entities' => $entitys,
            'config' => $this->getConfig()
        );
    }

    /**
     * Finds and displays a Noticia entity.
     *
     * @Route("/habilitar-noticias/guardar-cambios", name="noticia_habilitar_guardar_cambios")
     * @Method("POST")
     */
    public function habilitarNoticiasGuardarCambiosAction() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        //obtener formulario
        $noticiasRequest = $request->request->get('noticia', null);
        $array = array();
        if (!is_null($noticiasRequest)) {
            foreach ($noticiasRequest as $key => $noticia) {
                $array[] = $key;
            }

            $noticias = $em->getRepository('CmsGaBackBundle:Noticia')->findNoticiaByArray($array);

            //actualizar cambios
            foreach ($noticias as $noticia) {
                $noticia->setPublicado(true);
                $em->persist($noticia);
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Actualizacion exitosa');
        }

        //reedirigir a la accion habilitar noticias
        return $this->redirect($this->generateUrl('noticia_habilitar'), 301);
    }

    /**
     * Autocomplete a Noticia entity.
     *
     * @Route("/autocomplete-forms/get-seccion-noticia", name="autocomplete_get_seccion_noticia")
     */
    public function getSeccionByNoticiaArchivo() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_CONTROL')) {
            $limitar = false;
        } else {
            $limitar = true;
        }

        $entities = $em->getRepository('CmsGaBackBundle:Seccion')->findSeccionByRole($term, $limitar);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    //Post in facebook para Create and Update
    private function postInFacebook($paginaFace, $entity) {
        if ($this->getUser()->getFacebookAccessToken()) {
            $config = array();
            $config['appId'] = $paginaFace->getConfigFace()->getAppId();
            $config['secret'] = $paginaFace->getConfigFace()->getSecret();
            $config['idPage'] = $paginaFace->getIdPage();
            $config['accessToken'] = $this->getUser()->getFacebookAccessToken();
            //$config['permissions'] = ['email', 'publish_actions', 'manage_pages', 'status_update'];
            $this->get('tecspro_facebook_api')->configure($config);
            $post = array();
            $post['link'] = $this->generateUrl('detalle_noticia', array('id' => $entity->getId()), true);
            $post['name'] = $entity->getTitulo();
            $post['caption'] = $entity->getTitulo();
            $post['description'] = $entity->getDescripcion();
            $post['message'] = $entity->getDescripcion();
            // $imagenes = $entity->getImagenes();
            // if ($imagenes->count() > 0) {
            //     $picture         = $imagenes[0]->getAbsolutePath();
            //     $post['picture'] = $picture;
            // }
            $idPostFace = $this->get('tecspro_facebook_api')->postInPage($post);
        } else {
            $this->get('session')->getFlashBag()->add('danger', 'Debe Conectar con Facebook para poder publicar.');
            $idPostFace = null;
        }

        return $idPostFace;
    }

}
