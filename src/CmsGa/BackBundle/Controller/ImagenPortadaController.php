<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\ImagenPortada;
use CmsGa\BackBundle\Form\ImagenPortadaType;
use CmsGa\BackBundle\Form\ImagenPortadaFilterType;

/**
 * ImagenPortada controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/imagenportada")
 */
class ImagenPortadaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/ImagenPortada.yml',
    );

    /**
     * Lists all ImagenPortada entities.
     *
     * @Route("/", name="admin_imagenportada")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ImagenPortadaFilterType();
        $config = $this->getConfig();
        $acedirname = $this->container->get('ace.dirname');
        $folder = "/uploads/" . $acedirname->getDirname() . "/imagen_portada/";

        return array(
            'config' => $config,
            'folder' => $folder,
        );
    }

    /**
     * Creates a new ImagenPortada entity.
     *
     * @Route("/", name="admin_imagenportada_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:ImagenPortada:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ImagenPortadaType($this->getDoctrine()->getManager());
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new ImagenPortada entity.
     *
     * @Route("/new", name="admin_imagenportada_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ImagenPortadaType($this->getDoctrine()->getManager());
        $response = parent::newAction();
        return $response;
    }

    /**
     * Finds and displays a ImagenPortada entity.
     *
     * @Route("/{id}", name="admin_imagenportada_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing ImagenPortada entity.
     *
     * @Route("/{id}/edit", name="admin_imagenportada_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ImagenPortadaType($this->getDoctrine()->getManager());
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing ImagenPortada entity.
     *
     * @Route("/{id}", name="admin_imagenportada_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:ImagenPortada:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ImagenPortadaType($this->getDoctrine()->getManager());
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a ImagenPortada entity.
     *
     * @Route("/{id}", name="admin_imagenportada_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter ImagenPortada.
     *
     * @Route("/exporter/{format}", name="admin_imagenportada_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a ImagenPortada entity.
     *
     * @Route("/autocomplete-forms/get-noticia", name="ImagenPortada_autocomplete_noticia")
     */
    public function getAutocompleteNoticia() {
        $options = array(
            'repository' => "CmsGaBackBundle:Noticia",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a ImagenPortada entity.
     *
     * @Route("/autocomplete-forms/get-calendario", name="ImagenPortada_autocomplete_calendario")
     */
    public function getAutocompleteCalendario() {
        $options = array(
            'repository' => "CmsGaCalendarioBundle:Calendario",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable ImagenPortada.
     *
     * @Route("/get-table/", name="admin_imagenportada_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Finds and displays a Noticia entity.
     *
     * @Route("/imagen-portada/guardar-cambios", name="admin_imagenportada_guardarCambios")
     * @Method("POST")
     */
    public function imagenPortadaGuardarCambiosAction() {

        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        //obtener formulario
        $imagenPortadaPublicadoRequest = $request->request->get('ImagenPortadaPublicado', null);
        $imagenPortadaOrdenRequest = $request->request->get('ImagenPortadaOrden', null);

        if (!is_null($imagenPortadaPublicadoRequest)) {
            $entitys = $em->getRepository('CmsGaBackBundle:ImagenPortada')->findImagenPortadaByArray(array_keys($imagenPortadaOrdenRequest));

            //actualizar cambios
            foreach ($entitys as $imagenPortada) {
                //publicado
                if (array_key_exists($imagenPortada->getId(), $imagenPortadaPublicadoRequest)) {
                    $publicado = true;
                    $imagenPortada->setPublicado($publicado);
                } else {
                    $publicado = false;
                    $imagenPortada->setPublicado(false);
                }
                //orden
                $imagenPortada->setOrden((int) $imagenPortadaOrdenRequest[$imagenPortada->getId()]);
                $em->persist($imagenPortada);
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Actualizacion exitosa');
        }

        //reedirigir a la accion habilitar noticias
        return $this->redirect($this->generateUrl('admin_imagenportada'), 301);
    }

}
