<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Plantilla;
use CmsGa\BackBundle\Form\PlantillaType;
use CmsGa\BackBundle\Form\PlantillaFilterType;

/**
 * Plantilla controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/plantilla")
 */
class PlantillaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Plantilla.yml',
    );

    /**
     * Lists all Plantilla entities.
     *
     * @Route("/", name="admin_plantilla")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new PlantillaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Plantilla entity.
     *
     * @Route("/", name="admin_plantilla_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Plantilla:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new PlantillaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Plantilla entity.
     *
     * @Route("/new", name="admin_plantilla_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new PlantillaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Plantilla entity.
     *
     * @Route("/{id}", name="admin_plantilla_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Plantilla entity.
     *
     * @Route("/{id}/edit", name="admin_plantilla_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new PlantillaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Plantilla entity.
     *
     * @Route("/{id}", name="admin_plantilla_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Plantilla:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new PlantillaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Plantilla entity.
     *
     * @Route("/{id}", name="admin_plantilla_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Plantilla.
     *
     * @Route("/exporter/{format}", name="admin_plantilla_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Plantilla entity.
     *
     * @Route("/autocomplete-forms/get-secciones", name="Plantilla_autocomplete_secciones")
     */
    public function getAutocompleteSeccion()
    {
        $options = array(
            'repository' => "CmsGaBackBundle:Seccion",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Plantilla.
     *
     * @Route("/get-table/", name="admin_plantilla_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}