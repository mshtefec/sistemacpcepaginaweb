<?php

namespace CmsGa\BackBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class Imagen
{
    protected $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setDirname($entity)
    {
        $acedirname = $this->container->get('ace.dirname');
        $dirname    = $acedirname->getDirname();
        if (method_exists($entity, 'setDirRealpaths')) {
            $entity->setDirRealpaths($dirname);
        }
        if (method_exists($entity, 'setUploadDir')) {
            $entity->setUploadDir($dirname);
        }
        if (method_exists($entity, 'getImagenes')) {
            foreach ($entity->getImagenes() as $imagen) {
                $imagen->setUploadDir($dirname);
            }
        }
        if (method_exists($entity, 'getImagenPortada')) {
            if (!is_null($entity->getImagenPortada())) {
                $entity->getImagenPortada()->setUploadDir($dirname);
            }
        }
        if (method_exists($entity, 'getArchivo')) {
            if (!is_null($entity->getArchivo())) {
                $entity->getArchivo()->setUploadDir($dirname);
            }
        }
    }
}
