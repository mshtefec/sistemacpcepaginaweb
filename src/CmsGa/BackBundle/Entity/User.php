<?php

namespace CmsGa\BackBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User.
 *
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="UserSeccion", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * */
    private $secciones;

    /**
     * @ORM\OneToMany(targetEntity="UserPageFace", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * */
    private $pages;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    protected $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    protected $facebookAccessToken;

    /**
     * Constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->secciones = new ArrayCollection();
    }

    /**
     * Add secciones
     *
     * @param \CmsGa\BackBundle\Entity\UserSeccion $secciones
     * @return User
     */
    public function addSeccione(\CmsGa\BackBundle\Entity\UserSeccion $secciones) {
        $secciones->setUser($this);
        $this->secciones[] = $secciones;

        return $this;
    }

    /**
     * Remove secciones
     *
     * @param \CmsGa\BackBundle\Entity\UserSeccion $secciones
     */
    public function removeSeccione(\CmsGa\BackBundle\Entity\UserSeccion $secciones) {
        $this->secciones->removeElement($secciones);
    }

    /**
     * Get secciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSecciones() {
        return $this->secciones;
    }

    /**
     * Get secciones.
     *
     * @return string
     */
    public function getSeccionesStringIN() {
        $seccionIds = array();
        foreach ($this->getSecciones()->toArray() as $key => $seccion) {
            if ($key == 0) {
                array_push($seccionIds, $seccion->getId());
            } else {
                array_push($seccionIds, $seccion->getId());
            }
        }

        return $seccionIds;
    }

    /**
     * Add pages
     *
     * @param \CmsGa\BackBundle\Entity\UserPageFace $pages
     * @return User
     */
    public function addPage(\CmsGa\BackBundle\Entity\UserPageFace $pages) {
        $pages->setUser($this);
        $this->pages[] = $pages;

        return $this;
    }

    /**
     * Remove pages
     *
     * @param \CmsGa\BackBundle\Entity\UserPageFace $pages
     */
    public function removePage(\CmsGa\BackBundle\Entity\UserPageFace $pages) {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    
        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
    
        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }
}