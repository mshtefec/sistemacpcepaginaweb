<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagenSeccion.
 *
 * @ORM\Table(name="imagen_seccion")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class ImagenSeccion extends Imagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Seccion", inversedBy="imagenes")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id", nullable=true)
     */
    private $seccion;
    private $dirname;

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.

        /*  if ($this->dirname) {
          $this->uploadDir = 'uploads/' . $this->dirname . '/imagen_seccion';
          $res = 'uploads/' . $this->dirname . '/imagen_seccion';
          } elseif ($this->id) {
          $res = 'uploads/' . $this->uploadDir . '/imagen_seccion';
          } else {
          $res = 'uploads/imagen_seccion';
          } */

        return 'uploads/'.$this->uploadDir.'/imagen_seccion';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return ImagenNoticia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set seccion.
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $seccion
     *
     * @return ImagenNoticia
     */
    public function setSeccion(\CmsGa\BackBundle\Entity\Seccion $seccion = null)
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion.
     *
     * @return \CmsGa\BackBundle\Entity\Seccion
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * Set dirname.
     *
     * @param string $dirname
     *
     * @return ImagenNoticia
     */
    public function setDirname($dirname)
    {
        $this->dirname = $dirname;

        return $this;
    }
}
