<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SeccionRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SeccionRepository extends EntityRepository {

    public function findSeccionAndItem($url) {
        return $this->createQueryBuilder('s')
                        ->select('s, i, ic, n')
                        ->leftJoin('s.item', 'i')
                        ->leftJoin('i.children', 'ic')
                        ->leftJoin('s.noticias', 'n', 'with', 'n.publicado = true')
                        ->where('s.url = :url')
                        ->andWhere('s.publicado = true')
                        // ->andWhere('i.level = :level')
                        ->setParameter('url', $url)
                        // ->setParameter('level', $level)
                        ->orderBy('i.orden', 'ASC')
                        ->addOrderBy('ic.orden', 'desc')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

    public function findLikeSecciones($texto) {
        return $this->createQueryBuilder('s')
                        ->select('s, item, i')
                        ->leftJoin('s.item', 'item')
                        ->where('item.name LIKE :texto')
                        ->andWhere('s.publicado = true')
                        ->orWhere('item.title LIKE :texto')
                        ->orWhere('s.contenido LIKE :texto')
                        ->setParameter('texto', '%' . $texto . '%')
                        ->leftJoin('s.imagenes', 'i')
                        ->addOrderBy('item.nameSlug', 'ASC')
                        // ->setMaxResults(10)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllSeccionPortadaOrderByNoticias() {
        return $this->createQueryBuilder('s')
                        ->select('s,n,ip')
                        ->leftJoin('s.noticias', 'n')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->orderBy('s.ordenListado', 'asc')
                        ->addOrderBy('n.fecha', 'desc')
                        ->where('s.portada = :p')
                        ->andWhere('n.publicado=true and n.portada=true')
                        ->andWhere('s.publicado = true')
                        ->setParameter('p', true)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findSeccion($term) {
        return $this->createQueryBuilder('s')
                        ->join('s.item', 'i')
                        ->join('i.menu', 'm')
                        ->where('s.permiteNoticias = true')
                        ->andWhere('s.publicado = true')
                        ->andWhere('i.title LIKE :t')
                        ->setParameter('t', '%' . $term . '%')
                        ->orderBy('i.name', 'ASC')
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findSeccionByRole($term, $limitar) {
        $qb = $this->createQueryBuilder('s')
                ->join('s.item', 'i')
                ->join('i.menu', 'm')
                ->where('s.permiteNoticias = true')
                ->andWhere('i.name LIKE :t')
                ->setParameter('t', '%' . $term . '%')
                ->orderBy('i.name', 'ASC')

        ;
        if ($limitar) {
            $qb
                    ->join('s.usuarios', 'u')
            ;
        }
        return $qb
                        ->getQuery()
                        ->getResult()
        ;
    }

}
