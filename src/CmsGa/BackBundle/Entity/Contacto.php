<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contacto.
 *
 * @ORM\Table(name="contacto")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\ContactoRepository")
 */
class Contacto {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_apellido", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombreYapellido;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "'{{ value }}' no es un correo valido.",
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="text")
     * @Assert\NotBlank()
     */
    private $mensaje;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Correo")
     * @ORM\JoinColumn(name="correo_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $correo;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombreYapellido.
     *
     * @param string $nombreYapellido
     *
     * @return Contacto
     */
    public function setNombreYapellido($nombreYapellido) {
        $this->nombreYapellido = $nombreYapellido;

        return $this;
    }

    /**
     * Get nombreYapellido.
     *
     * @return string
     */
    public function getNombreYapellido() {
        return $this->nombreYapellido;
    }

    /**
     * Set telefono.
     *
     * @param string $telefono
     *
     * @return Contacto
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono.
     *
     * @return string
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Contacto
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set mensaje.
     *
     * @param string $mensaje
     *
     * @return Contacto
     */
    public function setMensaje($mensaje) {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje.
     *
     * @return string
     */
    public function getMensaje() {
        return $this->mensaje;
    }

    /**
     * Set correo.
     *
     * @param \CmsGa\BackBundle\Entity\Correo $correo
     *
     * @return Contacto
     */
    public function setCorreo(\CmsGa\BackBundle\Entity\Correo $correo = null) {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo.
     *
     * @return \CmsGa\BackBundle\Entity\Correo
     */
    public function getCorreo() {
        return $this->correo;
    }

}
