<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Noticia Repository.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class NoticiaRepository extends EntityRepository {

    public function findNoticia($id) {
        return $this->createQueryBuilder('n')
                        ->select('n,p,ip,i,v,s,ct')
                        ->leftJoin('n.archivo', 'p')
                        ->leftJoin('p.categoria', 'ct')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->leftJoin('n.seccion', 's')
                        ->where('n.id = :id')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

    public function queryAllOrderedByFecha() {
        return $this->createQueryBuilder('n')
                        ->select('n, c, ip, i, v')
                        ->where('n.publicado = true')
                        ->leftJoin('n.categoria', 'c')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
        ;
    }

    public function findAllOrderedByFecha($max = 6) {
        $query = $this->queryAllOrderedByFecha();

        return $query
                        ->setMaxResults($max)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function queryAllBecasOrderedByFecha() {
        return $this->createQueryBuilder('n')
                        ->select('n, c, ip, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('c.tipo = :becas')
                        ->setParameter('becas', 'becas')
                        ->leftJoin('n.categoria', 'c')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
        ;
    }

    public function queryAllOrderByFechaDesc($sec) {
        return $this->createQueryBuilder('n')
                        ->select('n,s')
                        ->Join('n.seccion', 's')
                        ->where('n.publicado = true')
                        ->andWhere("s.url  = '" . $sec . "'")
                        ->addOrderBy('n.fecha', 'DESC')
        ;
    }

    public function queryAllOrderedByCategoriaAndFecha() {
        // $qb  = $this->getEntityManager()->createQueryBuilder();
        // $qbN = $this->getEntityManager()->createQueryBuilder();
        // $qbA = $this->getEntityManager()->createQueryBuilder();
        // $qbI = $this->getEntityManager()->createQueryBuilder();
        // $qbN->select('nn.id')
        //     ->from('CmsGaBackBundle:Noticia', 'nn')
        //     ->leftJoin('nn.categoria', 'cn')
        //     ->where('cn.tipo = ?1')
        //     ->andWhere('nn.publicado = true')
        // ;
        // $qbA->select('na.id')
        //     ->from('CmsGaBackBundle:Noticia', 'na')
        //     ->leftJoin('na.categoria', 'ca')
        //     ->where('ca.tipo = ?2')
        //     ->andWhere('na.publicado = true')
        // ;
        // $qbI->select('ni.id')
        //     ->from('CmsGaBackBundle:Noticia', 'ni')
        //     ->leftJoin('ni.categoria', 'ci')
        //     ->where('ci.tipo = ?3')
        //     ->andWhere('ni.publicado = true')
        // ;
        // $qb->select('n')
        //     ->from('CmsGaBackBundle:Noticia', 'n')
        //     ->leftJoin('n.categoria', 'c')
        //     ->where($qb->expr()->In('n.id', $qbN->getDQL()))
        //     ->orWhere($qb->expr()->In('n.id', $qbA->getDQL()))
        //     ->orWhere($qb->expr()->In('n.id', $qbI->getDQL()))
        // ;
        // return $qb
        //     ->setParameter(1, 'noticias')
        //     ->setParameter(2, 'academicas')
        //     ->setParameter(3, 'investigaciones')
        //     ->addOrderBy('n.categoria', 'ASC')
        //     ->addOrderBy('n.fecha', 'DESC')
        // ;

        return $this->createQueryBuilder('n')
                        ->select('n, c, ip, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('c.tipo = :noticias OR c.tipo = :academicas OR c.tipo = :investigaciones')
                        ->setParameter('noticias', 'noticias')
                        ->setParameter('academicas', 'academicas')
                        ->setParameter('investigaciones', 'investigaciones')
                        ->leftJoin('n.categoria', 'c')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
        ;
    }

    public function findAllOrderedByCategoriaAndFecha() {
        $query = $this->queryAllOrderedByCategoriaAndFecha();

        return $query
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllNoticias($max = 10) {
        return $this->createQueryBuilder('n')
                        ->select('n, i, v')
                        ->where('n.publicado = true')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->setMaxResults($max)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findNoticiasDestacadas($max = 2) {
        return $this->createQueryBuilder('n')
                        ->select('n, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('n.destacada = true')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->setMaxResults($max)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllNoticiasInPortada($max = 60) {
        return $this->createQueryBuilder('n')
                        ->select('n, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('n.portada = true')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->orderBy('n.fecha', 'DESC')
                        ->setMaxResults($max)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllAcademicas() {
        return $this->createQueryBuilder('n')
                        ->select('n, c, ip, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('c.tipo = :noticias')
                        ->setParameter('noticias', 'academicas')
                        ->leftJoin('n.categoria', 'c')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->setMaxResults(5)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllInvestigaciones() {
        return $this->createQueryBuilder('n')
                        ->select('n, c, ip, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('c.tipo = :noticias')
                        ->setParameter('noticias', 'Investigaciones')
                        ->leftJoin('n.categoria', 'c')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->setMaxResults(5)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findAllNoticiasPortada() {
        return $this->createQueryBuilder('n')
                        ->select('n')
                        ->leftJoin('n.imagenPortada', 'ip')
                        ->where('n.portada = true')
                        ->andWhere('n.publicado = true')
                        ->orderBy('n.seccion')
                        ->addOrderBy('n.fecha', 'DESC')
                        ->setMaxResults(6)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findLikeNoticias($texto) {
        return $this->createQueryBuilder('n')
                        ->select('n, i, v')
                        ->where('n.publicado = true')
                        ->andWhere('n.titulo LIKE :texto')
                        ->orWhere('n.descripcion LIKE :texto')
                        ->orWhere('n.contenido LIKE :texto')
                        ->setParameter('texto', '%' . $texto . '%')
                        ->leftJoin('n.imagenes', 'i')
                        ->leftJoin('n.videos', 'v')
                        ->addOrderBy('n.fecha', 'DESC')
                        // ->setMaxResults(10)
                        ->getQuery()
                        ->getResult()
        ;
    }

    public function findNoticiaByArray($array) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('a')
                ->from('CmsGa\BackBundle\Entity\Noticia', 'a')
                ->where($qb->expr()->in('a.id', $array))
        ;

        return $qb
                        ->getQuery()
                        ->getResult();
    }

    public function findNoticiaPorAutorizar() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
                ->select('a')
                ->from('CmsGa\BackBundle\Entity\Noticia', 'a')
                ->where('a.publicado=false')
                ->orderBy('a.fecha','desc')
        ;
        
        return $qb
                        ->getQuery()
                        ->getResult();
    }

    public function findNoticiaPublicada($id) {
        return $this->createQueryBuilder('n')
                        ->select('n,p')
                        ->join('n.seccion', 's')
                        ->where('n.id = :id')
                        ->andWhere('n.publicado=true')
                        ->andWhere('s.publicado=true')
                        ->leftJoin('n.archivo', 'p')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getOneOrNullResult()
        ;
    }

}
