<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPageFace
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\UserPageFaceRepository")
 */
class UserPageFace implements \CmsGa\BackBundle\Filter\userSeccionInterface{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="pages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="PageFace", inversedBy="users")
     * @ORM\JoinColumn(name="pageFace_id", referencedColumnName="id")
     * */
    private $pageFace;

   


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \CmsGa\BackBundle\Entity\User $user
     * @return UserPageFace
     */
    public function setUser(\CmsGa\BackBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CmsGa\BackBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set pageFace
     *
     * @param \CmsGa\BackBundle\Entity\PageFace $pageFace
     * @return UserPageFace
     */
    public function setPageFace(\CmsGa\BackBundle\Entity\PageFace $pageFace = null)
    {
        $this->pageFace = $pageFace;

        return $this;
    }

    /**
     * Get pageFace
     *
     * @return \CmsGa\BackBundle\Entity\PageFace 
     */
    public function getPageFace()
    {
        return $this->pageFace;
    }
}
