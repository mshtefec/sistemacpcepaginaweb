<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagenNoticia.
 *
 * @ORM\Table(name="imagen_noticia")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ImagenNoticia extends Imagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Noticia", inversedBy="imagenes")
     * @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     */
    private $noticia;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.

        /* if ($this->dirname) {
          $this->uploadDir = 'uploads/' . $this->dirname . '/imagen_noticia';
          $res = 'uploads/' . $this->dirname . '/imagen_noticia';
          } elseif ($this->id) {
          $res = 'uploads/' . $this->uploadDir . '/imagen_noticia';
          } else {
          $res = 'uploads/imagen_noticia';
          } */

        return 'uploads/'.$this->uploadDir.'/imagen_noticia';
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return ImagenNoticia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set noticia.
     *
     * @param \CmsGa\BackBundle\Entity\Noticia $noticia
     *
     * @return ImagenNoticia
     */
    public function setNoticia(\CmsGa\BackBundle\Entity\Noticia $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia.
     *
     * @return \CmsGa\BackBundle\Entity\Noticia
     */
    public function getNoticia()
    {
        return $this->noticia;
    }
}
