<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use CmsGa\BackBundle\Entity\ImagenNoticia;

use Symfony\Component\HttpFoundation\File\File;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Noticia.
 *
 * @ORM\Table(name="noticia")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\NoticiaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Noticia {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=true)
     */
    private $contenido;

    /**
     * @var publicado
     *
     * @ORM\Column(name="publicado", type="boolean", nullable=true)
     */
    private $publicado;

    /**
     * @var portada
     *
     * @ORM\Column(name="portada", type="boolean", nullable=true)
     */
    private $portada;

    /**
     * @var destacada
     *
     * @ORM\Column(name="destacada", type="boolean", nullable=true)
     */
    private $destacada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     *  @ORM\OneToMany(targetEntity="ImagenNoticia", mappedBy="noticia", cascade={"all"}, orphanRemoval=true)
     */
    private $imagenes;

    /**
     * @var int
     * 
     * @ORM\OneTomany(targetEntity="VideoNoticia",mappedBy="noticia", cascade={"all"}, orphanRemoval=true)    
     */
    private $videos;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Seccion", inversedBy="noticias")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id")
     */
    private $seccion;

    /**
     * @ORM\OneToOne(targetEntity="ImagenPortada", inversedBy="noticia",cascade={"all"})
     * @ORM\JoinColumn(name="imagenPortada_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $imagenPortada;

    /**
     *  @ORM\OneToOne(targetEntity="CmsGa\RepositoryFilesBundle\Entity\Archivo", cascade={"all"}, orphanRemoval=true)
     *  @ORM\JoinColumn(name="archivo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $archivo;

    /**
     * @var string
     *
     * @ORM\Column(name="realpaths", type="text", nullable=true)
     */
    private $realpaths;

    private $dirRealpaths;


    /**
     * Constructor.
     */
    public function __construct() {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->videos   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setFecha(new \DateTime('now'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo.
     *
     * @param string $titulo
     *
     * @return Noticia
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo.
     *
     * @return string
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Noticia
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set contenido.
     *
     * @param string $contenido
     *
     * @return Noticia
     */
    public function setContenido($contenido) {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido.
     *
     * @return string
     */
    public function getContenido() {
        return $this->contenido;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Noticia
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Noticia
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set publicado.
     *
     * @param bool $publicado
     *
     * @return Noticia
     */
    public function setPublicado($publicado) {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado.
     *
     * @return bool
     */
    public function getPublicado() {
        return $this->publicado;
    }

    /**
     * Set portada.
     *
     * @param bool $portada
     *
     * @return Noticia
     */
    public function setPortada($portada) {
        $this->portada = $portada;

        return $this;
    }

    /**
     * Get portada.
     *
     * @return bool
     */
    public function getPortada() {
        return $this->portada;
    }

    /**
     * Set destacada.
     *
     * @param bool $destacada
     *
     * @return Noticia
     */
    public function setDestacada($destacada) {
        $this->destacada = $destacada;

        return $this;
    }

    /**
     * Get destacada.
     *
     * @return bool
     */
    public function getDestacada() {
        return $this->destacada;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Noticia
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Add imagenes.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenNoticia $imagenes
     *
     * @return Noticia
     */
    public function addImagene(\CmsGa\BackBundle\Entity\ImagenNoticia $imagenes) {
        $imagenes->setNoticia($this);
        $this->imagenes[] = $imagenes;

        return $this;
    }

    /**
     * Remove imagenes.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenNoticia $imagenes
     */
    public function removeImagene(\CmsGa\BackBundle\Entity\ImagenNoticia $imagenes) {
        $this->imagenes->removeElement($imagenes);
    }

    /**
     * Get imagenes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add videos.
     *
     * @param \CmsGa\BackBundle\Entity\VideoNoticia $videos
     *
     * @return Noticia
     */
    public function addVideo(\CmsGa\BackBundle\Entity\VideoNoticia $videos) {
        $videos->setNoticia($this);
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos.
     *
     * @param \CmsGa\BackBundle\Entity\VideoNoticia $videos
     */
    public function removeVideo(\CmsGa\BackBundle\Entity\VideoNoticia $videos) {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos() {
        return $this->videos;
    }

    /**
     * Set seccion.
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $seccion
     *
     * @return Noticia
     */
    public function setSeccion(\CmsGa\BackBundle\Entity\Seccion $seccion = null) {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion.
     *
     * @return \CmsGa\BackBundle\Entity\Seccion
     */
    public function getSeccion() {
        return $this->seccion;
    }

    /**
     * Set imagenPortada.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenPortada $imagenPortada
     *
     * @return Noticia
     */
    public function setImagenPortada(\CmsGa\BackBundle\Entity\ImagenPortada $imagenPortada = null) {
        $this->imagenPortada = $imagenPortada;

        return $this;
    }

    /**
     * Get imagenPortada.
     *
     * @return \CmsGa\BackBundle\Entity\ImagenPortada
     */
    public function getImagenPortada() {
        return $this->imagenPortada;
    }

    /**
     * Set archivo.
     *
     * @param \CmsGa\RepositoryFilesBundle\Entity\Archivo $archivo
     *
     * @return Noticia
     */
    public function setArchivo(\CmsGa\RepositoryFilesBundle\Entity\Archivo $archivo = null) {
        $this->archivo = $archivo;

        return $this;
    }

    /**
     * Get archivo.
     *
     * @return \CmsGa\RepositoryFilesBundle\Entity\Archivo
     */
    public function getArchivo() {
        return $this->archivo;
    }

    /**
     * Set realpaths.
     *
     * @param string $realpaths
     *
     * @return Noticia
     */
    public function setRealpaths($realpaths) {
        $this->realpaths = $realpaths;

        return $this;
    }

    /**
     * Get realpaths.
     *
     * @return string
     */
    public function getRealpaths() {
        return $this->realpaths;
    }

    public function setDirRealpaths($dirRealpaths) {
        $this->dirRealpaths = $dirRealpaths;
    }

    public function getDirRealpaths() {
        return $this->dirRealpaths;
    }

    /**
     * @ORM\PreFlush()
     */
    public function preUploadRealpaths()
    {
        $fs = new Filesystem();

        if (!is_null($this->getRealpaths())) {
            $arrayrealpaths = explode(",", $this->realpaths);
            foreach ($arrayrealpaths as $realpaths) {
                if ($fs->exists($realpaths)) {//si existe el archivo entra
                    $file          = new File($realpaths);
                    
                    $imagenNoticia = new ImagenNoticia();
                    $imagenNoticia->setFilePath($file->getFilename());
                    $imagenNoticia->setUploadDir($this->getDirRealpaths());

                    $path          = $imagenNoticia->getUploadRootDirBaseFile();

                    $file->move($path, $file->getFilename());

                    $this->addImagene($imagenNoticia);
                }
            }
        }
    }
}