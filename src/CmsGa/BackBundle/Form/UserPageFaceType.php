<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserPageFaceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('pageFace', 'select2', array(
                    'label' => 'Páginas Facebook',
                    'class' => 'CmsGa\BackBundle\Entity\PageFace',
                    'url' => 'autocomplete_get_pages',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\UserPageFace'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_userpageface';
    }

}
