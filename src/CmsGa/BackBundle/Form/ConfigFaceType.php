<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use CmsGa\BackBundle\Form\PageFaceType;

/**
 * ConfigFaceType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ConfigFaceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name')
            ->add('appId')
            ->add('secret')
            ->add('pages', 'collection', array(
                'type'         => new PageFaceType(),
                'label'        => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\ConfigFace'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_configface';
    }

}
