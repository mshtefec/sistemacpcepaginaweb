<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ImagenNoticiaType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ImagenNoticiaType extends AbstractType
{
    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'file', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPath',
                    'label' => 'Imagen',
                    'show_path'=>true
                )
            )
            ->add(
                'descripcion', 'textarea', array(
                    'label' => 'Descripción',
                    'required' => false,
                )
            );
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'CmsGa\BackBundle\Entity\ImagenNoticia',
            )
        );
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_imagennoticia';
    }
}
