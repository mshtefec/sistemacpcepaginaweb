<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ImagenSeccionType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ImagenSeccionType extends AbstractType
{
    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'file', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPath',
                    'label' => 'Imagen',
                )
            )
            ->add(
                'descripcion', 'textarea', array(
                    'label' => 'Descripción',
                )
            );
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'data_class' => 'CmsGa\BackBundle\Entity\ImagenSeccion',
            )
        );
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_imagennoticia';
    }
}
