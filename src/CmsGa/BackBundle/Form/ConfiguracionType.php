<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ConfiguracionType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ConfiguracionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave')
            ->add('valor')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Configuracion',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_configuracion';
    }
}
