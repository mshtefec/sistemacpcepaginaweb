<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;

/**
 * SeccionFilterType filtro.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class SeccionFilterType extends AbstractType
{
    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('menu', 'filter_entity', array(
                'class' => 'MWSimpleDynamicMenuBundle:Menu',
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                    // add conditions you need :)
                    if ('' !== $values['value'] && null !== $values['value']) {
                        $filterQuery->getQueryBuilder()->andWhere("m.name = '".$values['value']."'");
                    }
                },
                'mapped' => false,
                'label' => 'Menú',
                'attr' => array('class' => 'span3'),
            ))
            ->add('itemTitle', 'filter_text', array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                    // add conditions you need :)
                    if ('' !== $values['value'] && null !== $values['value']) {
                        $expr = $filterQuery->getExpressionBuilder();
                        $filterQuery->getQueryBuilder()->andWhere($expr->stringLike('i.title', $values['value']));
                    }
                },
                'mapped' => false,
                'label' => 'Título corto',
                'attr' => array('class' => 'span5'),
            ))
            ->add('publicado', 'filter_choice', array(
                'choices' => array(
                    1 => 'Publicado',
                    0 => 'No publicado',
                ),
                'attr' => array('class' => 'span2'),
            ))
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Seccion',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_seccionfiltertype';
    }
}
