<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * NoticiaFilterType filtro.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class NoticiaFilterType extends AbstractType
{
    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', 'filter_text', array(
                'label' => 'Título',
                'attr' => array('class' => 'span3'),
            ))
            ->add('descripcion', 'filter_text', array(
                'label' => 'Descripción',
                'attr' => array('class' => 'span5'),
            ))
            ->add('publicado', 'filter_choice', array(
                'choices' => array(
                    1 => 'Publicado',
                    0 => 'No publicado',
                ),
                'attr' => array('class' => 'span2'),
            ))
        ;

        $listener = function (FormEvent $event) {
            // Is data empty?
            foreach ((array) $event->getForm()->getData() as $data) {
                if (is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Noticia',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_noticiafiltertype';
    }
}
