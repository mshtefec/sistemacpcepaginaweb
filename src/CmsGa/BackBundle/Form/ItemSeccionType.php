<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

/**
 * ItemType form.
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class ItemSeccionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', null, array(
                    'label' => 'Título corto para menu',
                ))
                ->add('name', null, array(
                    'label' => 'Nombre detallado',
                ))
                // ->add('level', null, array(
                //     'label' => 'Nivel',
                //     'attr' => array(
                //         'class' => 'span1'
                //     )
                // ))
                ->add('orden', null, array(
                    'label' => 'Orden',
                ))
                ->add('menu', null, array(
                    'label' => 'Pertenece al menu',
                ))
                ->add('parent', 'select2', array(
                    'label' => 'Pertenece a item',
                    'class' => 'MWSimpleDynamicMenuBundle:Item',
                    'url' => 'autocomplete_get_parent_item',
                    'required' => false,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
        // ->add('seccion')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'MWSimple\DynamicMenuBundle\Entity\Item',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mwsimple_dynamicmenubundle_item';
    }

}
