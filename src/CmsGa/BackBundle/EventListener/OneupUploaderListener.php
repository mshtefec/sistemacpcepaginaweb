<?php

namespace CmsGa\BackBundle\EventListener;

use Oneup\UploaderBundle\Event\PostPersistEvent;

class OneupUploaderListener
{
    public function onUpload(PostPersistEvent $event)
    {
        $file      = $event->getFile();
        $realpath  = $file->getRealpath();
        
        $response              = $event->getResponse();
        $response['realpath']  = $realpath;
    }
}