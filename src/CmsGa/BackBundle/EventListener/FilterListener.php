<?php

namespace CmsGa\BackBundle\EventListener;

use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class FilterListener {

    private $ROLE_ADMIN = "ROLE_ADMIN";

    /**
     * @var string
     */
    protected $session;

    /**
     * @var string
     */
    protected $em;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @param SecurityContext $securityContext
     * @param Router $router The router
     * @param EntityManager $em
     */
    public function __construct(Session $session, EntityManager $em, TokenStorage $tokenStorage) {
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest() {
        if ($this->getUser() != "anon." && !is_null($this->getUser())) {
            $existe=array_search($this->ROLE_ADMIN, $this->getUser()->getRoles());
            if (is_bool($existe) && !$existe) {
                $this->em->getConfiguration()->addFilter('user_filter', 'CmsGa\BackBundle\Filter\UserFilter');
                $filter = $this->em->getFilters()->enable('user_filter');
                $filter->setParameter('user', (int) $this->getUser()->getId());
            }
        }
    }

    public function getUser() {
        if (!is_null($this->tokenStorage->getToken())) {
            return $this->tokenStorage->getToken()->getUser();
        }
        return null;
    }
}