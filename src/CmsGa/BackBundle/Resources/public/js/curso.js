var options = { 
    target:  '.disertanteSelect',
    success: updateDisertanteSelect 
}
jQuery('.disertanteForm').ajaxForm(options); 
function updateDisertanteSelect(response, statusText, xhr, $form) {
    jQuery('.disertanteSelect').append('<option value="'+response.id+'">'+response.text+'</option>');
    jQuery('.disertanteSelect').val(response.id);
    jQuery('#disertanteModal').modal('hide')
    console.log(response);
}