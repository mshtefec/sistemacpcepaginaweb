/**     
 * Validate
 *        
 */       
jQuery(function(){
    configValid = {                
        errorClass: "label label-important",
        highlight: function(element, errorClass) { 
            jQuery(element)
                .closest('.control-group')
                .removeClass('success')
                .addClass(errorClass);
              },
        /*success: function(element) {
            element
                .text('OK!')
                .addClass('valid')
                .closest('.control-group')
                .removeClass(errorClass)
                .addClass('label label-success');
        },     */ 
        ignore: ".ignore, .select2-offscreen, .select2-input"
    }                            
    $form = jQuery('.proyectoFrom');
    $form.validate(configValid);
})
