/**
 *  Borra la imgen form 
 *  
 *  @param Button $deleteButton DOM Borrar    
 * 
 *  @return void
 */
function deleteRow($deleteButton) { 
    var $divContent = jQuery($deleteButton).parent().parent().parent();                                    
    $divContent.remove();      
}    
function focusout($iframeSrc) {
    var value = jQuery($iframeSrc).val();
    jQuery($iframeSrc).parent().parent().parent().find('.iframeContent').html(value);
}


/**
 *  Documento se carga on ready y bindea las funciones
 * 
 * @return void
 */
jQuery(document).ready(function() {
    tinymce.init({
            selector: ".contenido",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect | fontselect | fontsizeselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            language : 'es'
         });
    collectionVideosHolder = jQuery('.videos-noticia');
    collectionImagenesHolder = jQuery('.imagenes-noticia');
    collectionVideosHolder.data('index', collectionVideosHolder.find(':input').length)
    collectionImagenesHolder.data('index', collectionImagenesHolder.find(':input').length)
    jQuery('.add-imagen-form').click(function(e) {
        e.preventDefault()
        addForm(collectionImagenesHolder, jQuery('.imagenes-noticia'));
    })
    jQuery('.add-video-form').click(function(e) {
        e.preventDefault()
        addForm(collectionVideosHolder, jQuery('.videos-noticia'));
        jQuery('.videos-noticia').delegate('.iframeSrc','paste', function(e) {
            focusout(this);
        });
        jQuery('.videos-noticia').delegate('.iframeSrc','focusout', function(e) {
            focusout(this);
        });
    })
    jQuery('.imagenes-noticia').delegate('.delete-form','click', function(e) {
        deleteRow(this); 
    });
    jQuery('.videos-noticia').delegate('.delete-form','click', function(e) {
        deleteRow(this); 
    });
    jQuery('.videos-noticia').delegate('.iframeSrc','paste', function(e) {
        focusout(this);
    });
    jQuery('.videos-noticia').delegate('.iframeSrc','focusout', function(e) {
        focusout(this);
     });
});
                                  
