$(document).ready(function() {
    Date.prototype.getDayName = function() {
        var dayNames = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        return dayNames[this.getDay()];
    };
    $.dateRangePickerLanguages =
    {
        'es':
            {
                'selected': 'Seleccionado:',
                'day':'Día',
                'days': 'Días',
                'apply': 'Cerrar',
                'week-1' : 'LU',
                'week-2' : 'MA',
                'week-3' : 'MI',
                'week-4' : 'JU',
                'week-5' : 'VI',
                'week-6' : 'SA',
                'week-7' : 'DO',
                'month-name': ['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIMBRE'],
                'shortcuts' : 'Accesos directos',
                'past': 'Pasado',
                'following':'Siguiente',
                'previous' : 'Anterior',
                'prev-week' : 'Semana',
                'prev-month' : 'Mes',
                'prev-year' : 'Año',
                'next':'Próximo',
                'next-week':'Semana',
                'next-month':'Mes',
                'next-year':'Año',
                'less-than' : 'Rango de fechas no debe ser más de %d days',
                'more-than' : 'Periodo no debe ser inferior a %d days',
                'default-more' : 'Por favor, seleccione un intervalo de fechas más largo que %d days',
                'default-less' : 'Por favor, seleccione un intervalo de fechas menos de %d days',
                'default-range' : 'Por favor, seleccione un intervalo de fechas entre %d and %d days',
                'default-default': 'Por favor, seleccione un intervalo de fechas'
            }
    };
    configObject = {
        format: 'MM/DD/YYYY',
        separator: ' hasta ',
        language: 'es',
        startOfWeek: 'sunday',// or monday
        getValue: function()
        {
            return this.value;
        },
        setValue: function(s)
        {
            this.value = s;
        },
        startDate: false,
        endDate: false,
        minDays: 0,
        maxDays: 0,
        showShortcuts: true,
        shortcuts: 
        {
            //'prev-days': [1,3,5,7],
            'next-days': [3,5,7],
            //'prev' : ['week','month','year'],
            'next' : ['week','month','year']
        },
        customShortcuts : []
    };
    $('#CmsGa_CalendarioBundle_evento_startEndDatetime')
        .dateRangePicker(configObject)
        .bind('datepicker-change',function(event,obj)
        {
            // obj will be something like this:
            // {
            //      date1: (Date object of the earlier date),
            //      date2: (Date object of the later date),
            //      value: "2013-06-05 to 2013-06-07"
            // }
            var theDay = obj.date1.getDayName();
            jQuery("#CmsGa_CalendarioBundle_evento_fechas_0_dia option").each(function(){
                $(this).attr("selected",false);
            });
            jQuery("#CmsGa_CalendarioBundle_evento_fechas_0_dia option[value="+theDay+"]").prop("selected","selected");
        })
    ;
    $('#CmsGa_CalendarioBundle_curso_startEndDatetime')
        .dateRangePicker(configObject)
        .bind('datepicker-change',function(event,obj)
        {
            // obj will be something like this:
            // {
            //      date1: (Date object of the earlier date),
            //      date2: (Date object of the later date),
            //      value: "2013-06-05 to 2013-06-07"
            // }
            var theDay = obj.date1.getDayName();
            jQuery("#CmsGa_CalendarioBundle_curso_fechas_0_dia option").each(function(){
                $(this).attr("selected",false);
            });
            jQuery("#CmsGa_CalendarioBundle_curso_fechas_0_dia option[value="+theDay+"]").prop("selected","selected");
        })
    ;
    $('#CmsGa_CalendarioBundle_actividad_startEndDatetime')
        .dateRangePicker(configObject)
        .bind('datepicker-change',function(event,obj)
        {
            // obj will be something like this:
            // {
            //      date1: (Date object of the earlier date),
            //      date2: (Date object of the later date),
            //      value: "2013-06-05 to 2013-06-07"
            // }
            var theDay = obj.date1.getDayName();
            jQuery("#CmsGa_CalendarioBundle_actividad_fechas_0_dia option").each(function(){
                $(this).attr("selected",false);
            });
            jQuery("#CmsGa_CalendarioBundle_actividad_fechas_0_dia option[value="+theDay+"]").prop("selected","selected");
        })
    ;
});