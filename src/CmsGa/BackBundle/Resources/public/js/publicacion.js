$(".autorForm").submit(function( event ) {
	// Stop form from submitting normally
	event.preventDefault();
	// Get some values from elements on the page:
	// var $form = $( this ),
	// term = $form.find( "input[name='s']" ).val(),
	// url = $form.attr( "action" );
	// // Send the data using post
	// var posting = $.post( url, { s: term } );
	// // Put the results in a div
	// posting.done(function( data ) {
	// 	var content = $( data ).find( "#content" );
	// 	$( "#result" ).empty().append( content );
	// });
	postForm($(this));
});

function postForm( $form ){
  /*
   * Get all form values
   */
  var values = {};
  $.each( $form.serializeArray(), function(i, field) {
    values[field.name] = field.value;
  });
 
  /*
   * Throw the form values to the server!
   */
  $.ajax({
    type        : $form.attr( 'method' ),
    url         : $form.attr( 'action' ),
    data        : values,
    success     : function(data) {
      $('#autorModal').modal('hide')
      // alert( data );
    }
  });
}
tinymce.init({
        selector: ".contenido",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
        ],
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
        language : 'es'
     });
