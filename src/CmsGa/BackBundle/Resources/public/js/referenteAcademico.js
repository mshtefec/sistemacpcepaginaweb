/**
 *  Borra la imgen form 
 *  
 *  @param Button $deleteButton DOM Borrar    
 * 
 *  @return void
 */
function deleteRow($deleteButton) { 
    var $divContent = jQuery($deleteButton).parent().parent();                                    
    $divContent.remove();      
}    
