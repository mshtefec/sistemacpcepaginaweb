/**
 * Funcion ajax on selectd pass element rebuild
 *
 *  @param string  url         url
 *  @param string  type        type post, get, etc..
 *  @param string  $classSelect element from DOM
 *  @param integer id          id from object
 *
 */
function ajaxElementOnSelected(url, type, $classSelect, id) {
    $.ajax({
        url: url,
        type: type,                                                                
        data: {id : id},
        success:
            function(data) {
                $classSelect.find('option').each(function () {
                        id=jQuery(this).val();
                        if (id !='') {                            
                            $classSelect.find("option[value='"+id+"']").remove();
                        }
                    });
                $.each(data, function(i, value) {
                    $classSelect.append($('<option>', {value: value.id, text: value.nombre}));
                });
            }
    }); 
}
