collectionPreguntasHolder = jQuery('.preguntas');
collectionPreguntasHolder.data('index', collectionPreguntasHolder.find(':input').length)
jQuery('.add-pregunta-form').click(function(e) {
    e.preventDefault();
    addForm(jQuery(this).parent().find('.preguntas'), jQuery(this).parent().find('.preguntas'));
    collectionOpcionesHolder = jQuery(this).parent().find('.preguntas').last().find('.opciones');
    collectionOpcionesHolder.data('index', 0);
    bindAddOpcion(); 
})
jQuery('.preguntas').delegate('.delete-form','click', function(e) {
	e.preventDefault();
    deleteRow(jQuery(this).parent()); 
});
function bindAddOpcion() {
    jQuery('.preguntas').find('.add-opcion-form').last().click(function(e) {
        e.preventDefault();
        addForm(jQuery(this).parent().find('.opciones'), jQuery(this).parent().find('.opciones').last(), /__opcion__/g);
    })
    jQuery('.opciones').delegate('.delete-form','click', function(e) {
        e.preventDefault();
        deleteRow(this); 
    });
}
jQuery(document).on('ready', function() {
    jQuery('.preguntas').find('.add-opcion-form').click(function(e) {
        e.preventDefault();
        addForm(jQuery(this).parent().find('.opciones'), jQuery(this).parent().find('.opciones').last(), /__opcion__/g);
    })
    jQuery('.opciones').delegate('.delete-form','click', function(e) {
        e.preventDefault();
        deleteRow(this); 
    });

})
