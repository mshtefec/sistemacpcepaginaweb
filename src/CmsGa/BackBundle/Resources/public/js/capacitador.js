/**
 *  Borra la imgen form 
 *  
 *  @param Button $deleteButton DOM Borrar    
 * 
 *  @return void
 */
function deleteRow($deleteButton) { 
    var $divContent = jQuery($deleteButton).parent().parent();                                    
    $divContent.remove();      
}    
/**
 * Tiny MCE config
 */
tinymce.init({
        selector: ".tinymceContent",
        language : 'es',
        setup: function(editor) {
            editor.on('change', function(e) {
                tinymce.triggerSave();
                console.log('change event');
            })
        }
});
jQuery('.selectEsEmpleado').on('change', function(){
    var isEmpleado = jQuery(this).val();
    if (isEmpleado == '1') {
        var content = jQuery('.contentEsEmpleado')
                        .data('prototype');
        jQuery('.contentEsEmpleado').html(content);
    } else {
        jQuery('.contentEsEmpleado').empty();
    }
})

jQuery('.areaGeneralContent').on('change', function(){
    var id = jQuery(this).val();
    var url= Routing.generate('ajax_tematica_especifica', { 'id': id});
    var type= 'POST';
    var classSelect = 'tematicasEspecificasContent';
    ajaxOnSelected(url, type, classSelect, id)
})
