

/**
 *  Borra la imgen form 
 *  
 *  @param Button $deleteButton DOM Borrar    
 * 
 *  @return void
 */
function deleteRow($deleteButton) { 
    var $divContent = jQuery($deleteButton).parent().parent();                                    
    $divContent.remove();      
}    
collectionIntegrantesHolder = jQuery('.integrantes-body');
collectionIntegrantesHolder.data('index', collectionIntegrantesHolder.find(':input').length)
collectionPalabrasClavesHolder = jQuery('.palabras-claves');
collectionPalabrasClavesHolder.data('index', collectionPalabrasClavesHolder.find(':input').length)
collectionBibliografiaHolder = jQuery('.bibliografia');
collectionBibliografiaHolder.data('index', collectionBibliografiaHolder.find(':input').length)
tinymce.init({
        selector: ".tinymceContent",
        language : 'es',
        setup: function(editor) {
            editor.on('change', function(e) {
                tinymce.triggerSave();
                console.log('change event');
            })
        }
});

jQuery('.tinymceContent').on('change', function(){
    var content = tinyMCE.activeEditor.getContent();
    console.log(content);
    jQuery(this).val(content);
})
jQuery('.add-integrante-form').click(function(e) {
    e.preventDefault()
    addForm(collectionIntegrantesHolder, jQuery('.integrantes-body'));
})
jQuery('.integrantes-body').delegate('.delete-form','click', function(e) {
    deleteRow(this); 
});
jQuery('.add-palabra-clave-form').click(function(e) {
    e.preventDefault()
    addForm(collectionPalabrasClavesHolder, jQuery('.palabras-claves'));
})
jQuery('.palabras-claves').delegate('.delete-form','click', function(e) {
    deleteRow(this); 
});
jQuery('.add-bibliografia-form').click(function(e) {
    e.preventDefault()
    addForm(collectionBibliografiaHolder, jQuery('.bibliografia'));
})
jQuery('.bibliografia').delegate('.delete-form','click', function(e) {

    deleteRow(jQuery(this).parent()); 
});

jQuery('.selectEsEmpleado').on('change', function(){
    var isEmpleado = jQuery(this).val();
    if (isEmpleado == '1') {
        var content = jQuery('.contentEsEmpleado')
                        .data('prototype');
        jQuery('.contentEsEmpleado').html(content);
    } else {
        jQuery('.contentEsEmpleado').empty();
    }
})

