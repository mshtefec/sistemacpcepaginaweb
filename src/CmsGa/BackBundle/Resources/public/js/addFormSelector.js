/**
 * Agrega formulario en tabla en forma de renglones
 *
 * @param collectionHolder variable global sotiene el prototype
 * @param $target          el DOM donde se inserta
 * @param id               id para el hidden del item seleccionado
 * @param text             texto del item seleccionado
 * 
 * @return void
 */
function addFormSelector(collectionHolder, target, regReplace) {
        // Get the data-prototype explained earlier
        var prototype = collectionHolder.data('prototype');
        //console.log(prototype, 'protopyte');
        // get the new index
        var index = collectionHolder.data('index');

        console.log(regReplace);
        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(regReplace, index);

        // increase the index with one for the next item
        collectionHolder.data('index', index + 1);  
        target.append(newForm);
}
