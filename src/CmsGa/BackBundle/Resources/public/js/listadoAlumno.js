jQuery('table tbody tr')
    .mouseenter(function() {
        jQuery( this ).addClass( "info" );
    })
    .mouseleave(function() {
        jQuery( this ).removeClass( "info" );
     });

jQuery('.btn-mini').on('click', function() {
    var estadoAnterior = jQuery(this).text();
    var estado = retrieveEstadoToPersist(estadoAnterior);
    var id = jQuery(this).attr('id');
    var url= Routing.generate('ajax_inscripcion', {'id': id, 'estado': estado});
    type = "GET";
    $clickButton = jQuery(this); 
    $.ajax({
        url: url,
        type: type,                                                                
        success:
            function(data) {
                update = retrieveEstadoToShow(data);
                $badge = $clickButton
                    .parent()
                    .parent()
                    .find('.badge');
                $badge.text(update.estado);
                $badge.attr('class','');
                $badge.attr('class', update.class);
                $clickButton.parent().find('.btn-mini');
                buttons = [],
                jQuery('.btn-mini').parent().find('.btn-mini').each(
                    function(){
                        buttons.push(jQuery(this).text());
                    });
                faltante = retrieveEstadoFaltante(buttons);
        console.log(faltante);
                updateButton = retrieveEstadoToChange(faltante);
                console.log(updateButton, 'update');
                $clickButton.text(updateButton.estado);
                $clickButton.attr('class',updateButton.class);    
            }
    }); 
})

/**
 * Retrieve Estado
 *
 * @param string estado estado nuevo
 *
 * @return string
 */
function retrieveEstadoToPersist(estado) {
    switch(estado) {
        case 'rechazar':
            return 'rechazado'
            break;
        case 'pre-inscribir':
            return 'preinscripto'
            break;
        case 'en proceso':
            return 'enproceso'
            break;
        case 'inscribir':
            return 'inscripto'
            break;
    }
};

/**
 * Retrieve Estado
 *
 * @param string estado estado nuevo
 *
 * @return string
 */
function retrieveEstadoToShow(estado) {
    switch(estado) {
        case 'rechazado':
            return {estado:'Rechazado', class: 'badge badge-important'};
            break;
        case 'preinscripto':
            return {estado:'Pre inscripto', class: 'badge badge-info'}
            break;
        case 'enproceso':
            return {estado:'En proceso', class: 'badge'}
            break;
        case 'inscripto':
            return {estado:'Inscripto', class: 'badge badge-success'}
            break;
    }
};

/**
 * Retrieve Estado Button
 *
 * @param string estado estado nuevo
 *
 * @return object
 */
function retrieveEstadoToChange(estado) {
    switch(estado) {
        case 'rechazar':
            return {estado:'rechazar', class: 'btn btn-mini btn-danger'};
            break;
        case 'pre-inscribir':
            return {estado:'pre-inscribir', class: 'btn btn-mini btn-info'}
            break;
        case 'en proceso':
            return {estado:'en proceso', class: 'btn btn-mini btn'}
            break;
        case 'inscribir':
            return {estado:'inscribir', class: 'btn btn-mini btn-success'}
            break;
    }
};
/**
 * Retrieve Estado Faltante
 *
 * @param array estados estado nuevo
 *
 * @return string estado
 */
function retrieveEstadoFaltante(buttons) {
    var estados = ['inscribir', 'en proceso', 'pre-inscribir','rechazar'];
    var elementDiff = arrayDiff(estados, buttons);
    console.log(elementDiff);
    return elementDiff[0];
};
/**
 * Array Diff
 */
function arrayDiff(array,arrayToCompare) {
    resgurado = [];
    array.forEach(function(key) {
        if (-1 === arrayToCompare.indexOf(key)) {
                    resgurado.push(key);
      }
    }, this);
    return resgurado;
};

