<?php

namespace CmsGa\BackBundle\Filter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Doctrine\ORM\Query\Filter\SQLFilter;
use Sistema\IRMBundle\Filter\negocioInterface;

/**
 * Description of negocioFilter
 *
 * @author rodrigo
 */
class UserFilter extends SQLFilter {

    public function addFilterConstraint(\Doctrine\ORM\Mapping\ClassMetadata $targetEntity, $targetTableAlias) {
        if ($targetEntity->reflClass->implementsInterface('CmsGa\BackBundle\Filter\userSeccionInterface')) {
            return sprintf('%s.%s = %s', $targetTableAlias, 'user_id', $this->getParameter('user'));
        } else {
            return '';
        }
    }

}
