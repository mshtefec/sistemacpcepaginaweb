<?php

// namespace CmsGa\BackBundle\DataFixtures\ORM;

// use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Doctrine\Common\Persistence\ObjectManager;
// use Symfony\Component\DependencyInjection\ContainerAwareInterface;
// use Symfony\Component\DependencyInjection\ContainerInterface;

// use CmsGa\BackBundle\Entity\Seccion;
// use MWSimple\DynamicMenuBundle\Entity\Item;

// class LoadSeccion extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
// {
//     /**
//      * @var ContainerInterface
//      */
//     private $container;

//     /**
//      * {@inheritDoc}
//      */
//     public function setContainer(ContainerInterface $container = null)
//     {
//         $this->container = $container;
//     }

//     /**
//      * {@inheritDoc}
//      */
//     public function load(ObjectManager $manager)
//     {
//         $institucional = array(
//             0 => array(
//                 'url' => 'menu_institucional',
//                 'contenido' => 'contenido',
//                 'menu' => $this->getReference('menu'),
//                 'name' => 'Institucional',
//                 'nameSlug' => 'institucional',
//                 'title' => 'Institucional',
//                 'level' => 1,
//                 'orden' => 1,
//             ),
//         );

//         foreach ($institucional as $key => $value) {
//             $seccion = new Seccion();
//             $seccion->setUrl($value['url']);
//             $seccion->setContenido($value['contenido']);
//             $item = new Item();
//             $item->setMenu($value['menu']);
//             $item->setName($value['name']);
//             $item->setNameSlug($value['nameSlug']);
//             $item->setTitle($value['title']);
//             $item->setLevel($value['level']);
//             $item->setOrden($value['orden']);
//             $seccion->setItem($item);
//             $manager->persist($seccion);
//             $this->addReference($value['url'], $item);
//         }

//         $items = array(
//             0 => array(
//                 'url' => 'menu_institucional_autoridades',
//                 'contenido' => 'autoridades',
//                 'menu' => $this->getReference('menu'),
//                 'name' => 'Autoridades',
//                 'nameSlug' => 'autoridades',
//                 'title' => 'Autoridades',
//                 'level' => 2,
//                 'orden' => 1,
//                 'parent' => $this->getReference('menu_institucional'),
//             ),
//         );

//         foreach ($items as $key => $value) {
//             $seccion = new Seccion();
//             $seccion->setUrl($value['url']);
//             $seccion->setContenido($value['contenido']);
//             $item = new Item();
//             $item->setMenu($value['menu']);
//             $item->setName($value['name']);
//             $item->setNameSlug($value['nameSlug']);
//             $item->setTitle($value['title']);
//             $item->setLevel($value['level']);
//             $item->setOrden($value['orden']);
//             $item->setParent($value['parent']);
//             $seccion->setItem($item);
//             $manager->persist($seccion);
//             $this->addReference($value['url'], $item);
//         }

//         $direcciones = array(
//             0 => array(
//                 'url' => 'direcciones_bienes',
//                 'contenido' => 'Bienes',
//                 'menu' => $this->getReference('direcciones'),
//                 'name' => 'Bienes',
//                 'nameSlug' => 'direccion_general_de_bienes',
//                 'title' => 'Bienes',
//                 'level' => 1,
//                 'orden' => 1,
//             ),
//         );

//         foreach ($direcciones as $key => $value) {
//             $seccion = new Seccion();
//             $seccion->setUrl($value['url']);
//             $seccion->setContenido($value['contenido']);
//             $item = new Item();
//             $item->setMenu($value['menu']);
//             $item->setName($value['name']);
//             $item->setNameSlug($value['nameSlug']);
//             $item->setTitle($value['title']);
//             $item->setLevel($value['level']);
//             $item->setOrden($value['orden']);
//             $seccion->setItem($item);
//             $manager->persist($seccion);
//             $this->addReference($value['url'], $item);
//         }

//         $enlaces = array(
//             0 => array(
//                 'url' => 'enlaces_centro-de-gestion',
//                 'contenido' => 'Centro de gestión',
//                 'menu' => $this->getReference('enlaces'),
//                 'name' => 'Centro de gestión',
//                 'nameSlug' => 'centro-de-gestion',
//                 'title' => 'Centro de gestión',
//                 'level' => 1,
//                 'orden' => 1,
//             ),
//         );

//         foreach ($enlaces as $key => $value) {
//             $seccion = new Seccion();
//             $seccion->setUrl($value['url']);
//             $seccion->setContenido($value['contenido']);
//             $item = new Item();
//             $item->setMenu($value['menu']);
//             $item->setName($value['name']);
//             $item->setNameSlug($value['nameSlug']);
//             $item->setTitle($value['title']);
//             $item->setLevel($value['level']);
//             $item->setOrden($value['orden']);
//             $seccion->setItem($item);
//             $manager->persist($seccion);
//             $this->addReference($value['url'], $item);
//         }

//         $manager->flush();
//     }

//     /**
//      * {@inheritDoc}
//      */
//     public function getOrder()
//     {
//         return 3; // the order in which fixtures will be loaded
//     }
// }

