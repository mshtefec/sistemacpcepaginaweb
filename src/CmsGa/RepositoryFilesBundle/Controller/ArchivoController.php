<?php

namespace CmsGa\RepositoryFilesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\RepositoryFilesBundle\Entity\Archivo;
use CmsGa\RepositoryFilesBundle\Form\ArchivoType;
use CmsGa\RepositoryFilesBundle\Form\ArchivoFilterType;

/**
 * Archivo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/archivo")
 */
class ArchivoController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/RepositoryFilesBundle/Resources/config/Archivo.yml',
    );

    /**
     * Lists all Archivo entities.
     *
     * @Route("/", name="admin_archivo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ArchivoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Archivo entity.
     *
     * @Route("/", name="admin_archivo_create")
     * @Method("POST")
     * @Template("MWSRFBundle:Archivo:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ArchivoType($this->get('security.authorization_checker'));
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Archivo entity.
     *
     * @Route("/new", name="admin_archivo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ArchivoType($this->get('security.authorization_checker'));
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Archivo entity.
     *
     * @Route("/{id}", name="admin_archivo_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Archivo entity.
     *
     * @Route("/{id}/edit", name="admin_archivo_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ArchivoType($this->get('security.authorization_checker'));
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Archivo entity.
     *
     * @Route("/{id}", name="admin_archivo_update")
     * @Method("PUT")
     * @Template("MWSRFBundle:Archivo:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ArchivoType($this->get('security.authorization_checker'));
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Archivo entity.
     *
     * @Route("/{id}", name="admin_archivo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Archivo.
     *
     * @Route("/exporter/{format}", name="admin_archivo_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Archivo entity.
     *
     * @Route("/autocomplete-forms/get-categoria", name="Archivo_autocomplete_categoria")
     */
    public function getAutocompleteCategoria() {
        $options = array(
            'repository' => "CmsGaRepositoryFilesBundle:Categoria",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Archivo.
     *
     * @Route("/get-table/", name="admin_archivo_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

}
