<?php

namespace CmsGa\RepositoryFilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CmsGa\toolsBundle\Entity\BaseFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Archivo.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CmsGa\RepositoryFilesBundle\Entity\ArchivoRepository")
 */
class Archivo extends BaseFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="archivos")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", nullable=false)
     */
    private $categoria;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @Assert\True(message = "Campo descripcion vacio")
     */
    public function isDescripcionNotBlank()
    {
        $res = true;
        if (is_null($this->getFile())) {
            $res = true;
        } elseif (empty($this->descripcion)) {
            $res = false;
        }

        return $res;
    }

    /**
     * @Assert\True(message = "Campo categoria vacio")
     */
    public function isCategoriaNotBlank()
    {
        $res = true;
        if (is_null($this->getFile())) {
            $res = true;
        } elseif (is_null($this->getCategoria())) {
            $res = false;
        }

        return $res;
    }

    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        // ladybug_dump_die($this->uploadDir);
        return 'uploads/'.$this->uploadDir.'/archivos';
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Archivo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set categoria.
     *
     * @param \MWSimple\Bundle\RepositoryFilesBundle\Entity\Categoria $categoria
     *
     * @return Archivo
     */
    public function setCategoria(\CmsGa\RepositoryFilesBundle\Entity\Categoria $categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria.
     *
     * @return \CmsGa\RepositoryFilesBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
