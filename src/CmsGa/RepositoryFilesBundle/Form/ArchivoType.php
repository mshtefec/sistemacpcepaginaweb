<?php

namespace CmsGa\RepositoryFilesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * ArchivoType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ArchivoType extends AbstractType {

    private $ROLE_ADMIN = "ROLE_ADMIN";
    private $ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
    private $ROLE_CONTROL = "ROLE_CONTROL";
    protected $authorizationChecker;
    private $limitar;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker) {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if ($this->authorizationChecker->isGranted($this->ROLE_CONTROL)) {
            $this->limitar = false;
        } else {
            $this->limitar = true;
        }
        $builder
                ->add('categoria', 'entity', array(
                    'class' => 'MWSRFBundle:Categoria',
                    'query_builder' => function (EntityRepository $er) {
                        $qb = $er->createQueryBuilder('c')
                                ->orderBy('c.nombre', 'ASC');
                        if ($this->limitar) {
                            $qb
                            ->leftJoin('c.secciones', 's')
                            ->join('s.usuarios', 'u')
                            ;
                        }
                        return $qb;
                    },
                    'empty_value' => 'Seleccione Categoria',
                ))
                ->add('descripcion', null, array(
                    'required' => false,
                ))
                ->add('file', 'mws_field_file', array(
                    'required' => true,
                    'label' => 'Archivo',
                    'file_path' => 'webPath',
                    'show_path' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\RepositoryFilesBundle\Entity\Archivo',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_bundle_repositoryfilesbundle_archivo';
    }

}
