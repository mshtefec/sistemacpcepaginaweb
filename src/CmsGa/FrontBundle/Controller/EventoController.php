<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class EventoController extends Controller {

    /**
     * Detalle Noticia.
     *
     * @param int $id id de noticia
     *
     * @Route("/detalle-evento/{id}", name="detalle_evento", defaults={"id" = 1})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleEventoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $evento = $em->getRepository('CmsGaCalendarioBundle:Evento')->find($id);

        return array(
            'entity' => $evento,
            'titulo' => 'Eventos',
        );
    }

    /**
     * @Route("/evento-listar/", name="listar_eventos")
     * @Template("@dirname/Actividad/listado.html.twig")
     */
    public function eventolistaAction() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('CmsGaCalendarioBundle:Evento')->findAllByActivosMenorAlDiaActual();
        $request = $this->getRequest();
        $pagina = $request->query->get('page', 1);

        $query = $em->createQuery($qb->getDQL());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $pagina/* page number */, 7/* limit per page */
        );
        $proximos = $em->getRepository('CmsGaCalendarioBundle:Evento')->findAllByActivosMayorIgualAlDiaActual();
        // parameters to template
        return array(
            'pagination' => $pagination,
            'titulo' => 'Eventos',
            'path_detalle' => 'detalle_evento',
            'proximos' => $proximos,
        );
    }

    /**
     * @Route("/evento-recent-listar/", name="listar_eventos_recientes")
     * @Template("@dirname/Actividad/recentList.html.twig")
     */
    public function recentEventosAction($max = 3) {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CmsGaCalendarioBundle:Evento')->findEventoByMax($max);

        return array(
            'entity' => $entities,
            'titulo' => 'Eventos',
            'subTituloAgenda' => 'Evento',
            'path_detalle' => 'detalle_evento',
        );
    }

    /**
     * @Route("/evento/{url}", name="detalle_eventobyurl", defaults={"url" = 0})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleByUrlAction($url) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CmsGaCalendarioBundle:Calendario')->findSegunUrl('evento/' . $url);

        return array(
            'entity' => current($entity),
            'titulo' => 'Eventos',
        );
    }

}
