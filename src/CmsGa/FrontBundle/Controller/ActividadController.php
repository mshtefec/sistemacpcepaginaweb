<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ActividadController extends Controller {

    /**
     * Detalle Noticia.
     *
     * @param int $id id de noticia
     *
     * @Route("/detalle-actividad/{id}", name="detalle_actividad", defaults={"id" = 1})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleActividadAction($id) {
        $em = $this->getDoctrine()->getManager();
        $actividad = $em->getRepository('CmsGaCalendarioBundle:Actividad')->find($id);

        return array(
            'entity' => $actividad,
            'titulo' => 'Actividades',
        );
    }

    /**
     * @Route("/actividad-listar/", name="listar_actividad")
     * @Template("@dirname/Actividad/listado.html.twig")
     */
    public function actividadlistaAction() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('CmsGaCalendarioBundle:Actividad')->findAllByActivosMenorAlDiaActual();
        $request = $this->getRequest();
        $pagina = $request->query->get('page', 1);

        $query = $em->createQuery($qb->getDQL());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $pagina/* page number */, 7/* limit per page */
        );
        $proximos = $em->getRepository('CmsGaCalendarioBundle:Actividad')->findAllByActivosMayorIgualAlDiaActual();
        // parameters to template
        return array(
            'pagination' => $pagination,
            'titulo' => 'Actividades',
            'path_detalle' => 'detalle_actividad',
            'proximos' => $proximos,
        );
    }

    /**
     * @Route("/actividad-recent-listar/", name="listar_actividad_recientes")
     * @Template("@dirname/Actividad/recentList.html.twig")
     */
    public function recentActividadesAction($max = 3) {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CmsGaCalendarioBundle:Actividad')->findActividadByMax($max);

        return array(
            'entity' => $entities,
            'titulo' => 'Actividades',
            'subTituloAgenda' => 'Actividad',
            'path_detalle' => 'detalle_actividad',
        );
    }

    /**
     * @Route("/actividad/{url}", name="detalle_actividadbyurl", defaults={"url" = 0})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleByUrlAction($url) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CmsGaCalendarioBundle:Calendario')->findSegunUrl('actividad/' . $url);

        return array(
            'entity' => current($entity),
            'titulo' => 'Actividades',
        );
    }

}
