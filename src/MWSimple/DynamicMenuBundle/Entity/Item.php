<?php

namespace MWSimple\DynamicMenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MWSimple\DynamicMenuBundle\Entity\ItemRepository")
 */
class Item
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="items")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", nullable=false)
     */
    private $menu;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_slug", type="string", length=255)
     */
    private $nameSlug;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Item", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity="CmsGa\BackBundle\Entity\Seccion", mappedBy="item", orphanRemoval=true)
     */
    private $seccion;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        $name = $this->getName();
        $menu = $this->getMenu();
        //obtengo el parent del item.
        $parent = $this->getParent();
        //si existe parent entra y utiliza el nameSlug del parent para la url.
        if (!empty($parent)) {
            $parent = $parent->getName();
            $name = $parent.'/'.$name;
        }
        if (!empty($menu)) {
            $menu = $menu->getName();
            $name = $menu.'/'.$name;
        }

        return $name;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameSlug.
     *
     * @param string $nameSlug
     *
     * @return Menu
     */
    public function setNameSlug($nameSlug)
    {
        $this->nameSlug = $nameSlug;

        return $this;
    }

    /**
     * Get nameSlug.
     *
     * @return string
     */
    public function getNameSlug()
    {
        return $this->nameSlug;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Item
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set level.
     *
     * @param int $level
     *
     * @return Item
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set orden.
     *
     * @param int $orden
     *
     * @return Item
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden.
     *
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set menu.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Menu $menu
     *
     * @return Item
     */
    public function setMenu(\MWSimple\DynamicMenuBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu.
     *
     * @return \MWSimple\DynamicMenuBundle\Entity\Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Add children.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $children
     *
     * @return Item
     */
    public function addChild(\MWSimple\DynamicMenuBundle\Entity\Item $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $children
     */
    public function removeChild(\MWSimple\DynamicMenuBundle\Entity\Item $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $parent
     *
     * @return Item
     */
    public function setParent(\MWSimple\DynamicMenuBundle\Entity\Item $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \MWSimple\DynamicMenuBundle\Entity\Item
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set seccion.
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $seccion
     *
     * @return Item
     */
    public function setSeccion(\CmsGa\BackBundle\Entity\Seccion $seccion = null)
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion.
     *
     * @return \CmsGa\BackBundle\Entity\Seccion
     */
    public function getSeccion()
    {
        return $this->seccion;
    }
}
