<?php

namespace MWSimple\DynamicMenuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * MenuType form.
 *
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 */
class MenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MWSimple\DynamicMenuBundle\Entity\Menu',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mwsimple_dynamicmenubundle_menu';
    }
}
