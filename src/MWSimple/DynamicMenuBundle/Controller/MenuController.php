<?php

namespace MWSimple\DynamicMenuBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MWSimple\DynamicMenuBundle\Entity\Menu;
use MWSimple\DynamicMenuBundle\Form\MenuType;
use MWSimple\DynamicMenuBundle\Form\MenuFilterType;
use CmsGa\BackBundle\Controller\Slug;

/**
 * Menu controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/menu")
 */
class MenuController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'MWSimple/DynamicMenuBundle/Resources/config/Menu.yml',
    );

    /**
     * Lists all Menu entities.
     *
     * @Route("/", name="admin_menu")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new MenuFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Menu entity.
     *
     * @Route("/", name="admin_menu_create")
     * @Method("POST")
     * @Template("MWSimpleDynamicMenuBundle:Menu:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new MenuType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $this->applySlug($entity);
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Menu entity.
     *
     * @Route("/new", name="admin_menu_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new MenuType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Menu entity.
     *
     * @Route("/{id}", name="admin_menu_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Menu entity.
     *
     * @Route("/{id}/edit", name="admin_menu_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new MenuType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Menu entity.
     *
     * @Route("/{id}", name="admin_menu_update")
     * @Method("PUT")
     * @Template("MWSimpleDynamicMenuBundle:Menu:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new MenuType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->applySlug($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Menu entity.
     *
     * @Route("/{id}", name="admin_menu_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Menu.
     *
     * @Route("/exporter/{format}", name="admin_menu_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Menu entity.
     *
     * @Route("/autocomplete-forms/get-items", name="Menu_autocomplete_items")
     */
    public function getAutocompleteItem() {
        $options = array(
            'repository' => "MWSimpleDynamicMenuBundle:Item",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Menu.
     *
     * @Route("/get-table/", name="admin_menu_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    private function applySlug($entity) {
        $entity->setNameSlug(Slug::slugify($entity->getName()));
    }

}
