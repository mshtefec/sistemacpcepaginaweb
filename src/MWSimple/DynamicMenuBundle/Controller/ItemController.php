<?php

namespace MWSimple\DynamicMenuBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MWSimple\DynamicMenuBundle\Entity\Item;
use MWSimple\DynamicMenuBundle\Form\ItemType;
use MWSimple\DynamicMenuBundle\Form\ItemFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Item controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/item")
 */
class ItemController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'MWSimple/DynamicMenuBundle/Resources/config/Item.yml',
    );

    /**
     * Lists all Item entities.
     *
     * @Route("/", name="admin_item")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ItemFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Item entity.
     *
     * @Route("/", name="admin_item_create")
     * @Method("POST")
     * @Template("MWSimpleDynamicMenuBundle:Item:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ItemType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Item entity.
     *
     * @Route("/new", name="admin_item_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ItemType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Item entity.
     *
     * @Route("/{id}", name="admin_item_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Item entity.
     *
     * @Route("/{id}/edit", name="admin_item_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ItemType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Item entity.
     *
     * @Route("/{id}", name="admin_item_update")
     * @Method("PUT")
     * @Template("MWSimpleDynamicMenuBundle:Item:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ItemType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Item entity.
     *
     * @Route("/{id}", name="admin_item_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Item.
     *
     * @Route("/exporter/{format}", name="admin_item_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Item entity.
     *
     * @Route("/autocomplete-forms/get-menu", name="Item_autocomplete_menu")
     */
    public function getAutocompleteMenu() {
        $options = array(
            'repository' => "MWSimpleDynamicMenuBundle:Menu",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Item entity.
     *
     * @Route("/autocomplete-forms/get-children", name="Item_autocomplete_children")
     */
    public function getAutocompleteItem() {
        $options = array(
            'repository' => "MWSimpleDynamicMenuBundle:Item",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Item entity.
     *
     * @Route("/autocomplete-forms/get-seccion", name="Item_autocomplete_seccion")
     */
    public function getAutocompleteSeccion() {
        $options = array(
            'repository' => "CmsGaBackBundle:Seccion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Item.
     *
     * @Route("/get-table/", name="admin_item_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-items-parent", name="autocomplete_get_parent_item")
     */
    public function getSeccionAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('MWSimpleDynamicMenuBundle:Item')->findLikeNameOrMenu($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
