<?php

namespace MWSimple\Bundle\CountVisitBundle\Services;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class CountVisits
{
    private $container;
    private $fs;
    private $finder;
    private $today;
    private $dirCount;
    private $file;

    public function __construct($container)
    {
        $this->container = $container;
        $this->fs = new Filesystem();
        $this->finder = new Finder();
        $this->today = new \DateTime('Today');
        $dir = $this->container->get('kernel')->getRootDir();
        $this->dirCount = $dir.'/count/';
        $dirname = 'cmsga';
        $this->file = $dirname.'_countvisits.txt';
    }

    public function countVisits()
    {
        $arrayToday = date_parse($this->today->format('d-m-Y'));
        $array = array(
            'total' => 1,
            $arrayToday['year'] => array(
                'total' => 1,
                $arrayToday['month'] => array(
                    'total' => 1,
                    $arrayToday['day'] => 1,
                ),
            ),
        );
        $serializedData = serialize($array);
        // si no existe el archivo total.txt entra y lo crea
        if (!$this->fs->exists($this->dirCount.$this->file)) {
            $this->fs->dumpFile($this->dirCount.$this->file, $serializedData);
        } else {
            $this->finder->files()
                ->in($this->dirCount)
                ->name($this->file)
            ;
            foreach ($this->finder as $file) {
                $recoveredData = unserialize($file->getContents());
                empty($recoveredData['total']) ? $recoveredData['total'] = 1 : $recoveredData['total'] += 1;
                empty($recoveredData[$arrayToday['year']]['total']) ? $recoveredData[$arrayToday['year']]['total'] = 1 : $recoveredData[$arrayToday['year']]['total'] += 1;
                empty($recoveredData[$arrayToday['year']][$arrayToday['month']]['total']) ? $recoveredData[$arrayToday['year']][$arrayToday['month']]['total'] = 1 : $recoveredData[$arrayToday['year']][$arrayToday['month']]['total'] += 1;
                empty($recoveredData[$arrayToday['year']][$arrayToday['month']][$arrayToday['day']]) ? $recoveredData[$arrayToday['year']][$arrayToday['month']][$arrayToday['day']] = 1 : $recoveredData[$arrayToday['year']][$arrayToday['month']][$arrayToday['day']] += 1;
                $serializedData = serialize($recoveredData);
                $this->fs->dumpFile($this->dirCount.$this->file, $serializedData);
            }
        }
    }

    public function getCountVisits()
    {
        $arrayToday = date_parse($this->today->format('d-m-Y'));
        if ($this->fs->exists($this->dirCount.$this->file)) {
            $this->finder->files()
                ->in($this->dirCount)
                ->name($this->file)
            ;
            foreach ($this->finder as $file) {
                $recoveredData = unserialize($file->getContents());
                $res = array(
                    'day' => $recoveredData[$arrayToday['year']][$arrayToday['month']][$arrayToday['day']],
                    'month' => $recoveredData[$arrayToday['year']][$arrayToday['month']]['total'],
                    'year' => $recoveredData[$arrayToday['year']]['total'],
                    'total' => $recoveredData['total'],
                );
            }
        } else {
            $res = null;
        }

        return $res;
    }
}
